% !TEX encoding = UTF-8

\def \testauthor{Lorenzo Luperi Baglini}

\makechapter
{\testauthor\astfootnote{\,Dipartimento di Matematica, 
Università degli Studi di Milano, Italia. 
\texttt{lorenzo.luperi@unimi.it} }}%autore
{Quanti elementi ha un insieme infinito?}%titolo
{Quanti elementi ha un insieme infinito? \\
\emph{\testauthor}}%tittoc
{Quanti elementi ha un insieme infinito?}%tittes
{Sin da bambini impariamo a contare il numero di elementi di una collezione 
di oggetti, abituandoci ad alcune evidenti proprietà immediate: se ad una 
collezione aggiungo oggetti, il loro numero cresce; se ne tolgo, il numero 
diminuisce; se unisco collezioni diverse, il numero totale di elementi si 
ottiene sommando, e così via. Queste proprietà evidenti rimangono vere se 
proviamo a contare il numero di elementi di un insieme infinito? La risposta 
dipende dal modo scelto per formalizzare la nozione di "quantità di 
elementi" di un insieme infinito. 
In questo intervento mostreremo come l'analisi nonstandard permetta di 
preservare varie proprietà intuitivamente evidenti tramite la nozione di 
"numerosità", e come questa sia connessa a quella di "insieme iperfinito". 
}%sommario

% \makecapitolo
% {Lorenzo Luperi Baglini\footnote{ Dipartimento di Matematica, 
% Università di Milano, Via Saldini 50, 20133 Milano, Italia. 
% \texttt{lorenzo.luperi@unimi.it} }}
% {Quanti elementi ha un insieme infinito?}{}{}
% {Sin da bambini impariamo a contare il numero di elementi di una collezione 
% di oggetti, abituandoci ad alcune evidenti proprietà immediate: se ad una 
% collezione aggiungo oggetti, il loro numero cresce; se ne tolgo, il numero 
% diminuisce; se unisco collezioni diverse, il numero totale di elementi si 
% ottiene sommando, e così via. Queste proprietà evidenti rimangono vere se 
% proviamo a contare il numero di elementi di un insieme infinito? La risposta 
% dipende dal modo scelto per formalizzare la nozione di "quantità di 
% elementi" di un insieme infinito. 
% In questo intervento mostreremo come l'analisi nonstandard permetta di 
% preservare varie proprietà intuitivamente evidenti tramite la nozione di 
% "numerosità", e come questa sia connessa a quella di "insieme iperfinito". 
% }

% \begin{center}
% \uppercase{\bf Quanti elementi ha un insieme infinito?}
% \vskip 20pt
% {\bf Lorenzo Luperi Baglini\footnote{Dipartimento di Matematica, 
% Universit\`{a} 
% di Milano, Via Saldini 50, 20133 Milano, Italia.}}\\
% 
% 
% {\tt lorenzo.luperi@unimi.it}\\
% \end{center}


\section{Introduzione}
Saper contare è una delle abilità basilari che permettono di confrontarci 
col mondo circostante. Intorno i due anni, impariamo a contare fino a dieci 
utilizzando le dita delle mani; una volta presa confidenza, iniziamo a 
contare direttamente gli oggetti uno dopo l'altro; infine, alle elementari 
impariamo somme e prodotti, che ci permettono di contare raggruppando. 
Qualsiasi sia il modo scelto, troviamo sempre lo stesso risultato finale 
(seppur non con la stessa efficienza).\par
Definire precisamente quale sia il numero di elementi di un insieme 
infinito, invece, non è altrettanto immediato. 
Per capire quali proprietà aspettarsi, 
possiamo appellarci ad alcune proprietà immediate degli insiemi finiti e 
provare a generalizzarle. 
Ad esempio, secondo Euclide una nozione di 
grandezza\footnote{\,Euclide parlava di grandezza di figure geometriche, 
intesa come aree e volumi delle stesse, ma le sue intuizioni si applicano 
bene anche al concetto di numero di elementi di un insieme.} dovrebbe 
soddisfare le proprietà seguenti:
  \begin{itemize}
	\item E1) Cose uguali ad una stessa cosa sono pure uguali tra loro;
	\item E2) Se uguali sono aggiunti ad uguali, i risultanti sono uguali;
	\item E3) Se uguali sono sottratti da uguali, i rimanenti sono uguali;
	\item E4) Oggetti che si applichino esattamente l'uno sull'altro hanno \\
stessa grandezza;
	\item E5) L'intero è più grande della parte.
\end{itemize}
 
Nel nostro contesto, i primi tre concetti hanno un'interpretazione chiara.
\par 
Il primo principio ci dice che la relazione binaria $R$ tra insiemi, per cui 
$R(A,B)$ vale se e solo se $A$ e $B$ hanno lo stesso numero di elementi, 
deve essere transitiva. Questo, unito alle richieste ovvie di simmetria e 
riflessività, ci dice che $R$ debba essere una relazione di equivalenza.\par
Il secondo e il terzo parlano di unioni e differenze insiemistiche: \\ 
se valgono 
$R(A_{1},B_{1})$ ed $R(A_{2},B_{2})$, con 
$A_{1}\cap A_{2}=B_{1}\cap B_{2}=\emptyset$, \\
allora dovrà valere anche 
$R(A_{1}~\cup~A_{2},~B_{1}~\cup~ B_{2})$, \\
e analogamente con la differenza insiemistica.\par
Più delicate sono le interpretazioni degli ultimi due principi. Infatti, se 
interpretati nel modo più semplice possibile, il quarto ci dice che due 
insiemi che possano essere messi in bigezione devono avere la stessa 
grandezza; il quinto, che un insieme deve avere più elementi di tutti i suoi 
sottoinsiemi propri. 
È però ben noto che questi due principi, così interpretati, sono 
incoerenti: famoso è l'esempio prodotto già da Galileo considerando naturali 
e quadrati. 
Infatti, $f(n)=n^{2}$ è una bigezione tra naturali e quadrati, 
dunque: 
se vale E4, non può valere E5; se vale E5, non può valere E4.\par 
Il padre delle teorie della grandezza per insiemi infiniti è Georg Ferdinand 
Ludwig Philipp Cantor che, nel periodo 1874-84, introduce le cardinalità.

\begin{definizione} \emph{Due insiemi $A,B$ hanno la stessa cardinalità 
(notazione $|\cdot |$) se e solo se esiste una bigezione $f:A\rightarrow B$. 
Si pone anche $|A|\leq |B|$ se esiste una funzione iniettiva $f:A\rightarrow 
B$.}
\end{definizione}

Il principio E4 è preservato nella sua interpretazione più naturale, e 
diviene definizione stessa della grandezza; è invece E5 a venire modificato, 
richiedendo che l'intero non sia più piccolo della parte:
\begin{itemize}
\item \(\text{E5}^{\prime}) \, A\subseteq B\Rightarrow |A|\leq |B|.\) 
\end{itemize}
Se rapportata al finito, questa teoria corrisponde a contare con le dita: la 
grandezza di un insieme è determinata mettendolo in bigezione con un altro 
insieme di cui conosciamo già il numero degli elementi.\par
Nel 1883 lo stesso Cantor introduce un'altra teoria della grandezza, la 
teoria 
dei numeri ordinali (poi approfondita da numerosi altri studiosi, tra cui 
Von Neumann). 
In questa teoria, ogni insieme viene pensato munito di una relazione 
di buon ordine. 
Identificando un sottoinsieme di un insieme bene ordinato col 
suo tipo d'ordine, il principio E4 viene interpretato come
\begin{itemize}
 \item \(\text{E4}^{\prime\prime})\)  Se  \(A,B\) sono in bigezione 
che preserva l'ordine, hanno lo stesso tipo d'ordine;
\end{itemize}
% \[ \text{E4}^{\prime\prime}) \text{ Se } A,B \text{ sono in bigezione che 
% preserva l'ordine, hanno lo stesso tipo d'ordine;}\]
mentre E5 viene interpretato come 
\begin{itemize}
\item \(\text{E5}^{\prime\prime})\) Se  \(A\) è un segmento iniziale proprio 
di \(B\) allora \(ot(A)<ot(B).\)
\end{itemize}

Le cardinalità possono essere identificate con una sottoclasse degli 
ordinali; 
ordinali diversi possono avere la stessa cardinalità. Al finito, questa 
teoria corrisponde a contare mettendo uno ad uno in successione gli 
elementi.\par
La rilevanza di queste due teorie non può essere sovrastimata; però, sia le 
operazioni di somma tra cardinali infiniti, che quelle tra ordinali, 
presentano proprietà ben lontane da quello che sarebbe superficialmente 
lecito aspettarsi. 
Ad esempio, se $\kappa,\iota$ sono cardinali infiniti si ha 
$\kappa+\iota=\kappa\iota=\max\{\kappa,\iota\}$ mentre, tra ordinali, vale 
ad esempio che\footnote{\,$\omega$ è l'ordinale che corrisponde ai numeri 
naturali (con 0) con l'usuale buon ordinamento.} $1+\omega=\omega<\omega 
+1$.\par
È quindi naturale porsi una domanda: è possibile una teoria della grandezza 
che sia più generale di quella degli ordinali ma con migliori proprietà 
algebriche?


\section{Somme infinite}

Volendo procedere nel parallelismo con i tre diversi modi di contare al 
finito, 
rimane da esplorare l'idea del contare raggruppando, e poi sommando. Avendo 
a che fare con insiemi infiniti, questo forza a considerare somme infinite. 
Introdurre una buona nozione di somma infinita è tutt'altro che immediato, 
soprattutto se si vogliono avere buone proprietà algebriche. Ad esempio, 
consideriamo le seguenti somme:

\begin{itemize}
\item $\sum_{n\in\mathbb{N}} 1=1+1+1+\dots$;
\item $\sum_{n\in\mathbb{N}} n=1+2+3+4+\dots$
\item $\sum_{n\in\mathbb{N}} (-1)^{n}=-1+1-1+1-1+\dots$;
\item $\sum_{n\in\mathbb{N}}(-1)^{n}n=-1+2-3+4-5+\dots$.
\end{itemize}

Nei primi due casi, è ragionevole avere un'idea dell'andamento relativo: ad 
esempio, per ogni $n\in\mathbb{N}$ la somma dei primi $n$ termini fa $n$ nel 
primo caso e $\frac{n(n+1)}{2}$ nel secondo, quindi è ragionevole aspettarsi 
che se la prima somma ha un qualche valore $\alpha$, la seconda debba valere 
$\frac{\alpha(\alpha+1)}{2}$. \par  

Gli ultimi due sono meno chiari, dato che raggruppamenti diversi danno luogo a 
somme diverse: ad esempio, a seconda che si scelga di raggruppare a due a 
due i termini nell'ultima somma a partire da un pari o da un dispari, si ha:  
\begin{itemize}
\item $(-1+2)+(-3+4)+(-5+6)+\dots=1+1+1+\dots =\alpha$;
\item $-1+(2-3)+(4-5)+\dots=-1-1-1-\dots=-\alpha$.
\end{itemize}

Come per l'introduzione della teoria della grandezza, anche per le somme 
infinite conviene dapprima isolare le proprietà che ci aspettiamo di poter 
ottenere, per poi provare a costruire un modello. 
Denoteremo con $\sum_{k\in K} a_{k}$ la somma infinita dei numeri reali 
$a_{k}$, dove $K$ è un insieme qualsiasi linearmente ordinato. 
Per semplicità, qua supporremo sempre 
$K\subseteq\mathbb{N}^{n}$ per un qualche $n\in\mathbb{N}$; con alcuni 
aggiustamenti tecnici, sarebbe possibile estendere la teoria in maniera 
analoga a quella che andremo ora ad esporre. \par 
Le somme infinite vogliamo che rispettino le regole seguenti:
\begin{itemize} [noitemsep]
\item {\bfseries Esistenza:} \\
Ogni somma transfinita $\sum_{k\in K}a_{k}$ 
esiste in un campo ordinato $\mathbb{K}$.
\item {\bfseries Finitezza:} \\
Se solo un numero finito degli $a_{k}$ è non nullo, 
la somma transfinita coincide con la somma finita.
\item {\bfseries Somma di somme:} \\
$\sum_{k\in K} a_{k}+\sum_{k\in K} b_{k}=
\sum_{k\in K} \left(a_{k}+b_{k}\right)$.
\item {\bfseries Confronto:} \\
Se da un certo $m$ in poi $\sum_{k=0}^{m}a_{k}< \sum_{k=0}^{m} b_{k}$ 
allora $\sum_{k\in K}a_{k}< \sum_{k\in K} b_{k}$.
\end{itemize}
 
Una conseguenza immediata di queste proprietà è la necessità di lavorare in 
un campo non Archimedeo, dato che $\sum_{k} 1=1+1+1+\dots\in\mathbb{K}$ è 
necessariamente più grande di qualsiasi numero naturale. Le altre proprietà 
sono tutte intuitive: la Finitezza ci dice che le somme infinite devono 
generalizzare quelle finite; la Somma di somme è una regola di 
associatività, e il Confronto è estremamente intuitivo (ma non banale: ad 
esempio, questa proprietà non vale per le serie!).\par
L'analisi nonstandard fornisce un modello ragionevole per gli assiomi qui 
sopra 
fissati. Infatti, se prendiamo un'ultrapotenza 
$^{\ast}\mathbb{R}=\mathbb{R}^{\mathbb{N}}_{\mathcal{U}}$, per le somme 
indicizzate da $\mathbb{N}$ basta porre
\[\sum_{k\in \mathbb{N}} a_{k}:=\left[\sum_{k=1}^{n} 
a_{k}\right]_{\mathcal{U}}\]
per avere facilmente tutte le proprietà qui sopra: 
\begin{itemize}[noitemsep]
\item {\bfseries Esistenza:} \\
$\sum_{k\in \mathbb{N}} a_{k}\in\,^{\ast}\mathbb{R}$, 
dato che il rappresentante di qualsiasi 
successione di reali è un iperreale;
\item {\bfseries Finitezza:} \\
Se $a_{k}=0$ da un certo indice $m$ in poi, 
$\left[\sum_{k=1}^{n} a_{k}\right]_{\mathcal{U}}=\sum_{k=1}^{m} a_{k}$, 
dato che identifichiamo ogni numero reale $r$ con la classe di equivalenza 
della successione costantemente uguale ad $r$;
\item {\bfseries Somma di somme:} \\
$\left[\sum_{k=1}^{n} 
a_{k}\right]_{\mathcal{U}}+\left[\sum_{k=1}^{n} 
b_{k}\right]_{\mathcal{U}}=\left[\sum_{k=1}^{n} 
a_{k}+b_{k}\right]_{\mathcal{U}}$ 
per definizione di somma tra iperreali;
\item {\bfseries Confronto:} \\
Se da un certo $m$ in poi 
$\sum_{k=1}^{m}a_{k}< \sum_{k=1}^{m} b_{k}$ allora 
$\left[\sum_{k=1}^{n} a_{k}\right]_{\mathcal{U}} < 
\left[\sum_{k=1}^{n} b_{k}\right]_{\mathcal{U}}$, 
per la definizione dell'ordinamento su $^{\ast}\mathbb{R}$.
\end{itemize}

Osserviamo che la definizione qui sopra ha una buona relazione con la più 
diffusa nozione di somma infinita, le serie: se $\sum_{k=1}^{\infty}a_{k}$ è 
una serie reale convergente, vale che
\[\sum_{k=1}^{\infty} a_{k}:=st\left(\sum_{k\in\mathbb{N}} a_{k}\right).\]


\section{Numerosità}

Le somme infinite ci danno subito una buona nozione di grandezza non 
Archimedea, le numerosità. 
Qui ci restringiamo al caso numerabile, studiato la prima volta 
da Vieri Benci in \cite{B1} e poi approfondito da Vieri Benci e Mauro Di 
Nasso in \cite{BDN1}. 
Una trattazione completa del caso numerabile si trova in 
\cite{Libro BDN}; per lo studio di casi non necessariamente numerabili 
rimandiamo ai riferimenti bibliografici. 

\begin{definizione} \emph{Sia $A\subseteq\mathbb{N}$, e sia 
$^{\ast}\mathbb{R}:=\mathbb{R}^{\mathbb{N}}_{\mathcal{U}}$. Ricordiamo che 
l'indicatrice di $A$ è la funzione $I_{A}:\mathbb{N}\rightarrow \mathbb{N}$ 
definita da}
\[I_{A}(k)=\begin{cases} 1, & \text{se } k\in A;\\
0, & \text{altrimenti.}
\end{cases}\]
\emph{La numerosità di $A$ è il numero ipernaturale}
\[num(A):=\left[\sum_{k\leq n} I_{A}(k)\right]_{\mathcal{U}}. \]
\end{definizione}

Questa definizione permette di formalizzare alcune proprietà intuitivamente 
vere. Infatti, poniamo 
\begin{center}
$num(\mathbb{N})=\left[n\right]_{\mathcal{U}}=\alpha$.
\end{center}
Intuitivamente, l'insieme $P$ dei numeri pari ha la metà degli elementi di 
$\mathbb{N}$. Con le numerosità, questa idea si può formalizzare: infatti 
\[ I_{P}(k)=\begin{cases}
\frac{k}{2}, & \text{se} \ k\in P;\\
\frac{k-1}{2}, & \text{se} \ k\notin P.
\end{cases}\]

Dunque se $P\in\U$, si ha che $num(P)=\frac{\alpha}{2}$; altrimenti, 
$num(P)=\frac{\alpha-1}{2}$. In ambo i casi, comunque, si ha che 
$st\left(\frac{num(N)}{num(P)}\right)=2$. In modi simili\footnote{\,Sempre 
con 
un'opportuna scelta dell'ultrafiltro.}, si può far vedere che 
\begin{itemize}
\item $num(\mathbb{Z})=2\alpha+1$ dato che $\mathbb{Z}=\mathbb{N}\cup 
-\mathbb{N} \cup \{0\}$;
\item $num(\{\text{quadrati}\})=\sqrt{\alpha}$, dato che per ogni $n$ 
quadrato 
ci sono $\sqrt{n}$ quadrati fino ad $n$;
\item $num(\mathbb{N}\times\mathbb{N})=\alpha^{2}$,
\end{itemize}

e così via. \par
Nella teoria delle numerosità, il principio E5 è immediatamente verificato 
nella 
sua interpretazione naturale, mentre E4 viene modificato come segue:
\begin{itemize}
 \item \(\text{E4}^{\prime\prime\prime})\) Se le indicatrici di \(A,B\) 
preservano i conteggi\footnote{\,Cioè se $\left\{n\in\mathbb{N}\mid 
\sum_{k=1}^{n}I_{A}(k)=\sum_{k=1}^{n}I_{B}(k)\right\}\in\mathcal{U}$.}, 
allora \\
\(num(A)=num(B)\).
\end{itemize}
% 
% \[ \text{E4}^{\prime\prime\prime})\text{ Se le indicatrici di }A,B\text{ 
% preservano i conteggi\footnote{\,Cioè se $\left\{n\in\mathbb{N}\mid 
% \sum_{k=1}^{n}I_{A}(k)=\sum_{k=1}^{n}I_{B}(k)\right\}\in\mathcal{U}$.} 
% allora } 
% num(A)=num(B).\]

\begin{esempio} Siano $P=\{\text{pari}\}$, $D=\{\text{dispari}\}$ e 
$P\in\mathcal{U}$. Basta osservare che per ogni $n$ pari si ha 
$\sum_{k=1}^{n} I_{P}(n)=\sum_{k=1}^{n} I_{D}(n)$, per concludere che \\
$num(A)=num(B)$. 
\end{esempio}

Concludiamo questa sezione collegando le numerosità alle cardinalità. 
Ricordiamo una delle possibili definizioni equivalenti di insieme 
iperfinito:

\begin{definizione} 
\emph{Un insieme $H\subseteq\,^{\ast}\mathbb{N}$ è iperfinito se è in 
bigezione interna con $[1,N]$ per un qualche $N\in\,^{\ast}\mathbb{N}$. 
In questo caso, $N$ è detto cardinalità interna di $H$ (notazione: $|H|$)}.  
\end{definizione}

Un'osservazione immediata è che, nel nostro modello con ultrapotenze, per 
ogni $A\subseteq\N$ si ha 

\[num(A):=|[1,\alpha]\cap\ ^{\ast}A|.\]

Questo mostra quindi che la numerosità non è altro che una particolare 
cardinalità iperfinita, che per transfer si comporta come una cardinalità 
finita. Quindi si può dire che le buone proprietà delle numerosità non sono 
altro che le buone proprietà delle cardinalità finite, estese ad insiemi 
infiniti tramite transfer ed estensioni iperfinite.


\section{Numerosità ed Ordinali}

È ben noto che si possano interpretare le cardinalità come particolari 
numeri ordinali, in modo da vedere gli ordinali come una generalizzazione 
delle cardinalità. 
Qui vogliamo spiegare brevemente come le numerosità possano, a loro 
volta, vedersi come una generalizzazione degli ordinali. In questa sezione, 
supponiamo di avere una teoria delle numerosità che permetta di definire il 
numero di elementi anche di insiemi di numerosità stesse (per i dettagli, 
rimandiamo a \cite{BLB}).

\begin{definizione} Definiamo l'insieme $Ord\subset Num$ delle numerosità 
ordinali come segue: $\tau \in Ord$ se e solo se 
\begin{equation*}
\tau = num\left( \Omega_{\tau }\right)
     = num\left(\{ x\in Ord\mid x<\tau\}\right).
\end{equation*}
\end{definizione}

Si vede subito che alcune numerosità sono numerosità ordinali, ad esempio 
tutti i numeri finiti. Infatti:
\begin{itemize}
\item $0\in Ord$, perché $\Omega_{0}=\emptyset$, e $num(\emptyset)=0$;  
\item $1\in Ord$, perché $\Omega_{1}=\{0\}$, e $num(\{0\})=1$;
\item in generale, $\forall n\in\mathbb{N} \ n\in Ord$, perché 
$\Omega_{n}=\{0,\dots,n-1\}$, quindi \\
$num\left(\Omega_{n}\right)=n$.
\end{itemize}

Però non tutte le numerosità sono numerosità ordinali: un esempio è 
$\alpha =num\left( \mathbb{N}\right)$. 
Infatti, se $\alpha$ fosse un ordinale, allora per definizione dovrebbe 
essere 
\begin{eqnarray*}
\alpha &=&num\left(\{ x\in Ord\mid x<num\left( \mathbb{N}\right)\} \right)
=num(\mathbb{N}_{0}) \\
&=&num(\mathbb{N}\cup\{ 0\} )=\alpha +1,
\end{eqnarray*}
il che è assurdo. 
Però le uguaglianze qui sopra mostrano che $\alpha +1$ è 
un ordinale (è il più piccolo ordinale infinito): 
\begin{equation*}
\alpha +1=num\left( \mathbb{N}_{0}\right) =num\left(\{x\in Ord\mid x<\alpha 
\}\right).
\end{equation*}%

Induttivamente, è facile provare che, per ogni $n\in\mathbb{N}$, anche 
$\alpha+1+n$ è una numerosità ordinale. Più in generale, è possibile 
dimostrare il seguente:

\begin{teorema}
Dati $\sigma ,\tau \in Ord$ vale:
\begin{eqnarray*}
num(\Omega _{\sigma })+num(\Omega _{\tau }) &=&%
num(\Omega _{\sigma +\tau });  \\
num(\Omega _{\sigma })\cdot num(\Omega _{\tau }) &=&%
num(\Omega _{\sigma \tau }).
\end{eqnarray*}

In particulare $Ord$ è chiuso per somme e prodotti.
\end{teorema}
Le operazioni tra numerosità ordinali sono la restrizione delle operazioni 
tra numerosità, che non sono altro che la restrizione alle numerosità delle 
operazioni tra ipernaturali; in particolare, quindi, sono commutative. 
Invece le operazioni tra ordinali di Cantor (che qui denoteremo come COrd e 
scriveremo sempre nella forma barrata $\overline{\sigma}$, per non 
confonderli con le numerosità ordinali) sappiamo non avere analoghe buone 
proprietà algebriche. 
Una domanda sorge spontanea: come è possibile?\par
Per capire a cosa corrispondano le operazioni tra numerosità ordinali, 
ricordiamo che ogni ordinale di Cantor $\bar{\sigma}$ ha un'unica forma 
normale, data da 
\begin{equation*}
\bar{\sigma}=\sum_{n=0}^{m}\bar{\omega}^{j_{n}}a_{n}
\end{equation*}%
dove $a_{n}\in \mathbb{N}$ e $n_{1}<n_{2}\Rightarrow j_{n_{1}}>j_{n_{2}}$. 
In forma normale, ogni ordinale è scritto come se fosse un polinomio in 
$\bar{\omega}$; in questa rappresentazione, simulando le operazioni tra 
polinomi si introducono le cosiddette operazioni naturali tra ordinali di 
Cantor in forma normale: se
\begin{equation*}
\bar{\sigma}=\sum_{n=0}^{m}\bar{\omega}^{j_{n}}a_{n}\,\,\ \mathrm{e}\,\,\ {%
\bar{\tau}}=\sum_{n=0}^{m}\bar{\omega}^{j_{n}}b_{n}   
\end{equation*}%
si pone 
\begin{equation*}
\bar{\sigma}\oplus {\bar{\tau}}=\sum_{n=0}^{m}\bar{\omega}^{j_{n}}\left(
a_{n}+b_{n}\right) \ \ \mathrm{e}\,\,\,\bar{\sigma}{\otimes \bar{\tau}}%
=\bigoplus_{n,h=0}^{m}a_{n}b_{h}\bar{\omega}^{j_{n}\oplus j_{h}}.
\end{equation*}

Sia ora $\Phi:Ord\rightarrow COrd$ la mappa che associa ad ogni numerosità 
ordinale il suo tipo d'ordine:

\begin{equation*}
\Phi \left( \tau \right)=ot(\Omega _{\tau }).
\end{equation*}

Il risultato che chiude il cerchio nelle relazioni tra cardinalità, ordinali 
e numerosità è il seguente:
\begin{teorema}
La mappa $\Phi$ costituisce un isomorfismo di semianelli tra 
$(Ord,+,\cdot )$ e $(COrd,\oplus ,\otimes )$, in particolare 
\begin{equation*}
\Phi \left( \sigma +\tau \right) = \bar{\sigma}\oplus \bar{\tau}, \ 
\Phi \left( \sigma \tau \right) = \bar{\sigma}\otimes \bar{\tau}.
\end{equation*}
\end{teorema}

In particolare, l'identificazione degli ordinali di Cantor con le numerosità 
ordinali permette di interpretare le operazioni naturali tra ordinali di 
Cantor in termini di grandezze di insiemi infiniti  a priori, anziché 
trattarle puramente in maniera algebrica; inoltre, mettendo insieme le varie 
relazioni discusse, si ha che le cardinalità si possono vedere come 
particolari numeri ordinali, i numeri ordinali si possono vedere come 
particolari numerosità, e le numerosità stesse possono vedersi come 
cardinalità nonstandard. 
Quindi le numerosità forniscono una teoria della grandezza che generalizza 
gli ordinali con migliori proprietà algebriche, rispondendo positivamente 
alla domanda che ci eravamo posti nell'introduzione.


\begin{thebibliography}{10}
 \phantomsection
 \addcontentsline{toc}{section}{Bibliografia}

\bibitem{B1} Benci V., \textit{I numeri e gli insiemi etichettati}, 
Conferenze del seminario di matematica dell'Università di Bari, vol.
261, Laterza, Bari 1995.

\bibitem{BDN1} Benci V., Di Nasso M., \textit{Numerosities of labelled
sets: a new way of counting}, Adv. Math. 21 (2003), pp. 505--67.

\bibitem{BDNF} Benci V., Di Nasso M., Forti M., \textit{An Aristotelian
notion of size}, Ann. Pure Appl. Logic 143 (2006), pp. 43--53.

\bibitem{Libro BDN} Benci V., Di Nasso M., \textit{How to measure the 
infinite: Mathematics with infinite and infinitesimal numbers}, World
Scientific, Singapore, 2018.

\bibitem{BLB} Benci V., Luperi Baglini L., \textit{Euclidean numbers and
numerosities}, in preparazione.

\bibitem{DNF} Di Nasso M., Forti M., \textit{Numerosities of point sets
over the real line}, Trans. Amer. Math. Soc. 362 (2010), pp. 5355--5371.

\bibitem{FMR} Forti M., Morana Roccasalvo G., \textit{Natural numerosities 
ofsets of tuples}, Trans.~Amer.~Math.~Soc. 367 (2015), pp. 275--292
\end{thebibliography}
