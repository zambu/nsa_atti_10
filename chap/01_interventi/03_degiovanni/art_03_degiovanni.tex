% !TEX encoding = UTF-8

\begin{comment}

Caro Bruno,
mi sembra tutto a posto a parte qualche dettaglio:

    Quanto alla modifica in terza pagina, io intendevo "Per questa 
(popolazione studentesca) mi sembra che l’Analisi non standard alla Nelson 
possa fare la sua parte", in cui avevo sottointeso "popolazione studentesca" 
e, per errore, mi è partito un "che";  se la mia formulazione è troppo 
ellittica, è meglio scrivere "Per questi studenti mi sembra che l’Analisi non 
standard alla Nelson possa fare la sua parte."
    Nel testo compare "Theorem" invece di "Teorema".
    Mi sono accorto che, quando introduco un insieme con la specificazione, 
talvolta uso la barra verticale e talvolta i due punti; sarebbe meglio 
uniformare, ad esempio sempre la barra verticale. 
    Ero incerto se inserire o meno la citazione di Von Neumann; se viene 
inserita, forse è megio che sia all'inizio.

Visto che avete già impaginato, forse è più semplice se mettete voi stessi in 
atto queste correzioni.
Al caso fatemi sapere se è invece meglio che rimandi io il fileTeX.
Grazie e un caro saluto,

Marco

\end{comment}

\def \testauthor{Marco Degiovanni}

\makechapter
{\testauthor\astfootnote{\,Dipartimento di Matematica e Fisica, 
 Università Cattolica del Sacro Cuore, 
 Via Trieste 17, 25121 Brescia, Italy.\\
 \emph{E-mail adress}: marco.degiovanni@unicatt.it}}% autore
{Alcune riflessioni sull'approccio all'Analisi non standard \\
secondo E. Nelson}% titolo
{Alcune riflessioni sull'approccio secondo E. Nelson \\
\emph{\testauthor}}% tittoc
{Alcune riflessioni sull'approccio secondo E. Nelson}% tittes
{\begin{flushright}
``In mathematics you don't understand things.\\ 
You just get used to them''.

(Von Neumann)
 \end{flushright}


L'approccio più seguito all'Analisi non standard, conforme alla prima 
formulazione, prevede di ampliare l'insieme dei numeri reali con l'aggiunta 
dei numeri iperreali. 
Da un certo punto di vista, si può però dire che l'insieme dei numeri reali è già molto grande, a fronte in particolare della finitezza dei ragionamenti matematici. 
L'approccio alla Nelson mira allora a riconoscere che già nell'ambito dei numeri reali sono presenti infinitesimi e numeri infinitamente grandi.
Altre riflessioni vengono allora suscitate. Esiste un solo tipo di finito o c'è una distinzione tra un finito "esplicito" e un finito che va gestito con ragionamenti paragonabili con quelli richiesti dall'infinito?
}%sommario

\section{Introduzione}
%
Nell'ambito dei primi risultati sui prodotti notevoli, tutti si
sono imbattuti in formule del tipo
\begin{alignat*}{3}
&(a+b)^2 &&\,\,= &&\,\,a^2 + 2ab + b^2\,,\\
&(a+b)^3 &&\,\,= &&\,\,a^3 + 3a^2b + 3ab^2 + b^3\,.
\end{alignat*}
A un livello più avanzato, avranno poi incontrato formule come
\[
(a+b)^n =
\sum\limits_{k=0}^n \cho{n}{k}\,a^k b^{n-k}\,.
\]
C'è un salto netto fra la tecnica richiesta per provare
le prime due formule e quella generale.
Nel caso di $(a+b)^2$, $(a+b)^3$, anche $(a+b)^4$, basta
sviluppare esplicitamente la potenza del binomio e prendere
atto del risultato.
La formula con l'esponente $n$ richiede invece  o di appellarsi
a qualche principio generale (il principio di induzione)
o di ricondursi ad esempio al calcolo combinatorio, scaricando 
su quest'ultimo i problemi legati al numero~$n$ generico.
\par
Oltre alla ben nota contrapposizione fra \emph{finito} e
\emph{infinito}, esiste anche una distinzione fra il
\emph{finito esplicitamente trattabile} e il \emph{finito
trattabile solo con principi generali}.
In questo secondo caso, il finito ha un sapore simile
all'infinito, anche se le proprietà a cui appellarsi non 
saranno le stesse per finito e infinito.
\par
L'approccio all'Analisi non standard di 
E.~Nelson~\cite{nelson} mette 
bene in evidenza, tra le altre cose, questo aspetto.
La specificità di questo approccio è che non viene 
modificato il panorama degli oggetti matematici, a
differenza di quanto avviene nell'approccio originale di 
A.~Robinson~\cite{mdrobinson}, che ad esempio introduce
un nuovo insieme $\IR$.
Come contropartita, Nelson introduce una nuova frase 
aperta (o predicato) applicabile a qualunque oggetto
matematico: \emph{$x$ è standard}.
\par
In un certo senso \emph{standard} presenta qualche
somiglianza con \emph{esplicitamente trattabile}.
Ad esempio il numero naturale $0$ è standard e, 
se $n$ è un numero naturale standard, allora anche $n+1$ 
è standard.
Ciononostante esistono numeri naturali non standard
e non esiste il più grande numero naturale standard.
D'altra parte abbiamo visto che esistono numeri naturali
esplicitamente gestibili, ma non tutti lo sono né
esiste il più grande numero naturale esplicitamente 
gestibile.
\par
Il fatto che non venga modificato il complesso degli 
oggetti matematici rende inoltre l'approccio di Nelson adatto,
a mio avviso, a una trattazione \emph{semiclassica}, in cui
i concetti classici sono introdotti da definizioni classiche,
salvo essere anche caratterizzati in modo non classico, in modo
da aprire la strada a dimostrazioni di tipo non classico.
Si tratta di un percorso raccomandabile, secondo me, con studenti
già abituati a $\varepsilon$ e $\delta$, per i quali il confronto
con le formulazioni classiche non sarebbe eludibile.
\par
Come esemplificazione, in queste pagine mi propongo di esporre
gli assiomi dell'approccio di Nelson in una forma semplificata,
ma sufficiente per moltissimi casi.
Più precisamente, invece dell'Assioma I di Nelson, verrà
introdotta una sua conseguenza.
\par
Dopo una prima parte generale, lo scopo è quello di fornire
gli strumenti per dimostrare i teoremi basilari sulle funzioni
continue di una variabile reale.
Specificamente verrà dimostrato il Teorema di Weierstrass,
ma altri risultati sarebbero ottenibili con gli stessi
strumenti.
\par
Un fatto che emerge con evidenza è che si passa dalle
proprietà generali dei numeri reali al risultato con 
pochi concetti e passi intermedi, quando il percorso 
classico comporta di introdurre estremo superiore ed 
inferiore, successioni e sottosuccessioni, tutte nozioni
che richiedono una adeguata assimilazione.
\par
In conclusione credo che esista una popolazione studentesca
che deve essere introdotta a \emph{entrambi} gli approcci
all'Analisi, quello classico e quello non standard.
Per questi studenti mi sembra che l’Analisi non 
standard alla Nelson possa fare la sua parte.


%--------------------------------------------------------------------

\section{Approccio alla Nelson}
\label{sect:nelson}
%
Nel seguito, tipici ambienti di riferimento saranno
l'insieme $\R$ dei numeri reali e il prodotto cartesiano
$\R\times\R$.
Vi sono inoltre alcune \emph{frasi aperte} (o \emph{predicati})
di carattere basilare, quali
\begin{gather*}
x\in X\,,\qquad A\subseteq X\,,
\qquad\text{con $X, x, A$ qualunque}\,,\\
z=x+y\,,\qquad z=xy\,,\qquad x\leq y\,,
\qquad\text{con $x,y,z$ numeri reali}\,.
\end{gather*}
Nell'approccio alla Nelson non viene modificato il quadro
degli oggetti matematici e vengono mantenute tutte le usuali
frasi aperte a cui siamo abituati.
In più viene introdotta una nuova
frase aperta in una variabile:
\[
\text{$x$ è standard}\,,
\]
dove $x$ è un qualunque oggetto matematico.
%
\begin{definizione}
Una frase aperta si dice
\emph{classica} se è esprimibile senza utilizzare 
questa nuova frase aperta.
\end{definizione}
%
\begin{osservazione}
Non bisogna confondere il termine \emph{standard}, che  è
attribuibile agli \emph{oggetti matematici} (elementi, insiemi,
funzioni, relazioni), con il termine \emph{classico}, che
è attribuibile alle \emph{frasi aperte}.
\begin{alignat*}{3}
&\text{standard}&&\qquad\longrightarrow
&&\qquad\text{oggetto matematico}\,,\\
&\text{classica}&&\qquad\longrightarrow
&&\qquad\text{frase aperta}\,.
\end{alignat*}
\end{osservazione}
%
Tutte le proprietà delle specifiche frasi aperte a cui
siamo abituati vengono ribadite tali e quali.
Occorre invece porre attenzione a quelle asserzioni
che contengono una premessa del tipo
\emph{sia $\mathcal{P}(x)$ una qualunque frase aperta},
perché il complesso delle frasi aperte è stato ampliato.
In particolare, il tipico \emph{schema di specificazione} 
suona nel modo seguente.
\par\bigskip\noindent
\textbf{Schema di specificazione.}
\emph{Sia $\mathcal{P}(x)$ una qualunque frase aperta classica.
Allora, per ogni insieme $X$, esiste uno ed un solo 
sottoinsieme $A$ di $X$ tale che
\[
\forall x\quad x\in A \,\Longleftrightarrow\,
(x\in X \,\wedge\, \mathcal{P}(x))\,.
\]
}
%
Al solito, tale sottoinsieme si denota con
\[
\left\{x\in X|~\mathcal{P}(x)\right\}\,.
\]
Si intende che le conseguenze della specificazione a cui
siamo classicamente abituati continuano a valere, dal 
momento che si ottengono applicando la specificazione
a frasi aperte classiche.
\par
La nuova formulazione dello schema di specificazione va 
percepita come un mancato potenziamento, non come un
indebolimento.
\par
Vediamo qualche esempio nell'ambito dei numeri naturali.
%
\begin{theorem}
\label{thm:ind}
Sia $A\subseteq\N$ tale che
\begin{gather*}
0\in A\,,\\
\forall n\quad n\in A\Longrightarrow (n+1)\in A\,.
\end{gather*}
Allora $A=\N$.
\end{theorem}
%
Dal momento che tale affermazione è
classicamente vera, rimane vera nel nuovo
contesto.
In particolare, non è necessario sapere a
priori se $A$ è o non è standard.
\par
Diverso è il discorso per il cosiddetto
\emph{principio di induzione}, che suona nel
modo seguente.
\par\bigskip\noindent
\textbf{Principio di induzione.}
\emph{Sia $\mathcal{P}(n)$ una qualunque frase aperta classica.
Supponiamo che si abbia
\begin{gather*}
\mathcal{P}(0)\,,\\
\forall n\in\N\quad \mathcal{P}(n)
\Longrightarrow \mathcal{P}(n+1)\,.
\end{gather*}
Allora risulta
\[
\forall n\in\N\quad \mathcal{P}(n)\,.
\]
}

In effetti il principio di induzione è riconducibile
al Teorema \ref{thm:ind} considerando, per mezzo dello
schema di specificazione, l'insieme
\[
A = \left\{n\in\N|~\mathcal{P}(n)\right\}\,,
\]
il che è legittimo se la frase aperta $\mathcal{P}(n)$
è classica.
\par
Dopo questa breve revisione delle proprietà più
usuali, vediamo gli specifici assiomi che vengono aggiunti
per poter maneggiare la nuova frase aperta
\emph{$x$ è standard}.
\par\bigskip\noindent
\textbf{Assioma T.}
\emph{Se una frase aperta classica, contenente solo parametri standard,
è vera quando le variabili assumono valori standard, 
allora è vera per ogni valore delle variabili.
\\
Se una frase aperta classica, contenente solo parametri standard,
è vera per almeno un valore delle variabili, 
allora è vera per almeno un valore standard delle variabili.
}
\par\bigskip
Quanto alla formulazione che coinvolge il quantificatore 
esistenziale, se oltre all'esistenza sussiste anche l'unicità,
quell'unico valore della variabile è standard.
Vale quindi un criterio di carattere generale.
%
\begin{theorem}
Se un insieme $X$ è determinato in modo classico e 
univoco a partire da insiemi standard (eventualmente
a partire da nessun insieme), allora $X$ è standard.
\end{theorem}
%
\begin{esempio}
L'insieme vuoto $\emptyset$ è standard,
perché è l'insieme $X$ determinato in modo classico 
e univoco da
\[
\forall x\quad x\not\in X\,.
\]
Attraverso opportune procedure insiemistiche classiche e 
univoche, è possibile costruire $\N$, $\Z$, $\Q$, $\R$,
$\C$, che sono quindi tutti insiemi standard, come anche
$\R\times\R$ e $\R\times\R\times\R$.
\par
Nella usuale teoria degli insiemi, relazioni e funzioni sono 
identificate con il loro grafico.
Ciò premesso, sono standard somma e prodotto in $\R$ e in $\C$.
Nel caso di $\R$ è standard anche l'ordinamento.
Questo significa che sono standard ad esempio gli insiemi
\begin{gather*}
\left\{(x,y,z)\in\R\times\R\times\R|~z=x+y\right\}\,,\\
\left\{(x,y,z)\in\R\times\R\times\R|~z=xy\right\}\,,\\
\left\{(x,y)\in\R\times\R|~x\leq y\right\}\,.
\end{gather*}
Infine sono standard gli elementi $0$ e $1$
(e anche $i$ in $\C$).
\par
In generale, se una funzione è standard (ossia è standard
il suo grafico), allora sono standard il suo dominio e la sua 
immagine.
\end{esempio}
%
Il mancato potenziamento dello schema di specificazione
viene compensato da un assioma che consente di gestire, 
in una certa misura, anche le frasi aperte non classiche.
\par\bigskip\noindent
\textbf{Assioma S.}
\emph{Sia $\mathcal{P}(x)$ una frase aperta qualunque.
Allora, per ogni insieme standard $X$, esiste uno ed un solo 
sottoinsieme standard $A$ di $X$ tale che
\[
\forall x\quad (x\in A\,\wedge\, \text{$x$ è standard})  
\,\Longleftrightarrow\,
(x\in X \,\wedge\, \mathcal{P}(x)\,\wedge\, \text{$x$ è standard})\,.
\]
}
\par\bigskip
Tale sottoinsieme si denota con
\[
{}^S\left\{x\in X|\,\,\mathcal{P}(x)\right\}
\]
ed ha la proprietà che i suoi elementi standard sono esattamente 
gli $x$ standard in~$X$ che soddisfano $\mathcal{P}(x)$.
\par
Cos\`\i\ come il principio di induzione si appoggiava sullo
schema di specificazione, una variante del principio di
induzione è riconducibile all'assioma S.
%
\begin{theorem}
\label{thm:indn}
Sia $\mathcal{P}(n)$ una frase aperta qualunque.
Supponiamo che si abbia
\begin{gather*}
\mathcal{P}(0)\,,\\
\forall n\in\N\quad 
(\mathcal{P}(n)\,\wedge\, \text{$n$ è standard})
\Longrightarrow \mathcal{P}(n+1)\,.
\end{gather*}
Allora risulta
\[
\forall n\in\N\quad 
\text{$n$ è standard}
\Longrightarrow \mathcal{P}(n)\,.
\]
\end{theorem}
%
\begin{proof}
Consideriamo, utilizzando l'Assioma S, il sottoinsieme
standard di $\N$
\[
A = {}^S\left\{n\in \N|\,\,\mathcal{P}(n)\right\}\,.
\]
Dal momento che $0$ è standard, risulta che $0\in A$.
Inoltre, se $n\in A$ e $n$ è standard, si 
ha~$\mathcal{P}(n)$, quindi $\mathcal{P}(n+1)$.
Essendo univocamente determinato in modo classico
da $n$, $+$ e $1$, anche $n+1$ è
standard, per cui $n+1\in A$.
\par
La frase aperta classica nella variabile $n$
\[
n\in A \Longrightarrow (n+1) \in A\,,
\]
contenente i parametri standard $+$, $1$ e $A$,
è vera per ogni valore standard di $n$, quindi
è vera per ogni valore di $n$ per l'Assioma T.
\par
Segue dal Teorema~\ref{thm:ind} che $A=\N$.
Se $n\in\N$ e $n$ è standard, si conclude che
vale~$\mathcal{P}(n)$.
\end{proof}
%
Vediamo qualche tipica conseguenza.
%
\begin{theorem}
\label{thm:mleqn}
Siano $m,n \in\N$ con $m\leq n$ e $n$ standard.
Allora $m$ è standard.
\end{theorem}
%
\begin{proof}
Sia $\mathcal{P}(n)$ la frase aperta
\[
\forall m\in\N\quad m\leq n \quad
\Longrightarrow\quad \text{$m$ è standard}\,.
\]
Poiché $0$ è standard, si ha $\mathcal{P}(0)$.
\par
Se $n\in\N$ e vale $\mathcal{P}(n)$,
allora ogni $m\leq n$ è standard.
In particolare, $n$ è standard, per cui 
anche $n+1$ è standard.
Allora ogni $m\leq n+1$ è standard, cioè
vale $\mathcal{P}(n+1)$.
\par
Se  $\mathcal{P}(n)$ fosse classica, si potrebbe
invocare il principio di induzione.
Non lo è, ma possiamo invocare il
Teorema~\ref{thm:indn}.
Allora $\mathcal{P}(n)$ è vera per ogni $n\in\N$
con $n$ standard, da cui la tesi.
\end{proof}
%
\begin{theorem}
Sia $F$ un insieme finito e standard.
Allora gli elementi di $F$ sono tutti standard.
\end{theorem}
%
\begin{proof}
Se $F=\emptyset$, l'affermazione è banalmente vera.
Altrimenti, sia $n\in\N$ il numero di elementi di $F$.
Essendo univocamente determinato in modo classico da $F$,
che è standard, il numero $n$ è standard per l'Assioma~$T$,
cos\`\i\ come è standard l'insieme
\[
\left\{m\in\N|~1\leq m\leq n\right\} \,,
\]
che è determinato da $\N$, $\leq$, $1$ e $n$,
\par
Esiste un'applicazione biiettiva
\[
\left\{m\in\N|~1\leq m\leq n\right\} \longrightarrow F\,.
\]
Sempre per l'assioma $T$, esiste
un'applicazione biiettiva e standard 
\[
f:\left\{m\in\N|~1\leq m\leq n\right\} \longrightarrow F\,.
\]
D'altronde ogni elemento
$m$ di $\{1,\ldots,n\}$ è standard
per il Teorema~\ref{thm:mleqn}.
Allora $f(m)$ è standard per ogni $m=1,\ldots,n$.
Ne segue che ogni elemento di $F$ è standard.
\end{proof}
%
Nell'approccio originale di Nelson~\cite{nelson},
oltre agli Assiomi T e S veniva introdotto l'Assioma~I 
di idealizzazione.
Si tratta di un assioma non semplice, già a livello di
enunciazione.
Tuttavia in moltissimi casi una sua conseguenza è sufficiente.
Allora assumeremo come assioma questa conseguenza, che chiameremo
Assioma I debole.
\par\bigskip\noindent
\textbf{Assioma I debole.}
\emph{Per ogni insieme $X$ esiste un sottoinsieme finito $F$
di $X$ tale che
\[
\forall x\in X\quad
(\text{$x$ è standard}) \Longrightarrow x\in F\,.
\]
}
\par\bigskip
Ne segue immediatamente che ogni insieme infinito contiene
elementi non standard.
Ad esempio $\N$ è standard ma, essendo infinito, 
contiene elementi non standard.
\par
Occorre prestare attenzione al fatto che, in generale,
l'insieme $F$ contiene anche elementi non standard,
non è costituito \emph{esattamente} dagli elementi
standard di $X$.
Né sarebbe legittimo, partendo da $F$, andare a considerare
\[
\left\{x\in F|~\text{$x$ è standard}\right\}\,,
\]
perché verrebbe applicato lo schema di specificazione
a una frase aperta non classica.
\par
A titolo di esempio, vediamo che cosa si può dire nel caso $X=\N$.
%
\begin{theorem}
Sia $F$ un sottoinsieme finito di $\N$ contenente tutti gli
elementi standard di~$\N$.
\par
Allora $\max F$ non è standard, per cui $F$
contiene anche elementi non standard.
Inoltre~$F$ stesso non è standard.
\end{theorem}
%
\begin{proof}
Dal momento che $0$ è standard, si ha $0\in F$, 
per cui $F\neq\emptyset$
ed è legittimo considerare $\max F$.
\par
Se per assurdo $\max F$ fosse standard, sarebbe standard 
anche $1+\max F$, perché determinato univocamente in modo
classico da $\max F$, $+$ e $1$.
Ne seguirebbe $1+\max F\in F$, il che è assurdo.
\par
Se poi $F$ fosse standard, seguirebbe che $\max F$ è standard,
perché determinato univocamente in modo
classico da $F$.
\end{proof}
%
A questo punto vale la pena citare due risultati di tipo 
fondamentale.
\par\medskip\noindent
%{\bf Metateorema 1.-}  TODO sistemare \bf
Metateorema 1.-
{\em La teoria degli insiemi ampliata con l'aggiunta degli 
assiomi~$T$, $S$ e $I$ debole è contraddittoria se e solo se 
l'usuale teoria degli insiemi è contraddittoria.}
\par\medskip\noindent
%{\bf Metateorema 2.-} TODO sistemare \bf
Metateorema 2-
{\em Se un'affermazione classica è dimostrabile nell'ambito della
teoria degli insiemi ampliata con l'aggiunta degli assiomi $T$,
$S$ e $I$ debole, allora essa è dimostrabile anche nell'ambito
dell'usuale teoria degli insiemi.}
\par\medskip
In sostanza, l'ampliamento della teoria che è stato effettuato 
non deve suscitare ulteriori preoccupazioni a 
livello di consistenza.
Inoltre, nell'ambito delle affermazioni classiche, le carte in 
tavola non vengono sostanzialmente cambiate.
Quello che capita è una semplificazione di alcune
dimostrazioni di affermazioni classiche, visto che si posseggono 
più strumenti, e la possibilità di introdurre nuove nozioni
(non classiche) e dimostrare nuove affermazioni (non classiche).


%--------------------------------------------------------------------

\section{Numeri infinitesimi e numeri infinitamente grandi}
\label{sect:analisi}
%
Dopo queste premesse di carattere generale, passiamo ad aspetti
più pertinenti con l'Analisi matematica.
%
\begin{definizione}
\label{defn:simeq}
Due numeri reali $x$ e $y$ si dicono  
{\em infinitamente vicini}
(in simboli $x\simeq y$), se si ha $|x-y| < \varepsilon$
per ogni $\varepsilon >0$ con $\varepsilon$ standard.
\end{definizione}
%
Il prossimo enunciato mostra che la nozione non è banale.
%
\begin{theorem}
\label{thm:simeqo}
Esiste $\hat{\delta} \in \R$ con $\hat{\delta}>0$ 
e $\hat{\delta} \simeq 0$.
\end{theorem}
\begin{proof}
Sia $F \subseteq ]0,+\infty[$ un insieme finito conforme
all'assioma $I$ debole.
Poiché~$1$ è standard, risulta $F\neq\emptyset$, 
per cui possiamo porre
\[
\hat{\delta} = \frac{1}{2} \, \min F\,.
\]
Se $\varepsilon>0$ è standard, si ha
$\varepsilon \in F$, per cui 
$0 < \hat{\delta} < \min F \leq \varepsilon$.
Risulta quindi $\hat{\delta} \simeq 0$.
\end{proof}
%
Evidentemente un tale $\hat{\delta}$ non è standard,
altrimenti si avrebbe
\[
|\hat{\delta} - 0| < \hat{\delta}\,,
\]
il che è assurdo.
\par
Il prossimo risultato afferma, adattato al nostro linguaggio,
che $\geq$ e $\leq$ passano al limite.
%
\begin{proposizione}
\label{prop:dis}
Siano $\xi,x$ ed $a$ tre numeri reali con $x$  ed $a$
standard, $\xi \simeq x$ e $\xi \geq a$ (risp. $\xi \leq a$).
\par
Allora risulta $x\geq a$ (risp. $x\leq a$).
\end{proposizione}
\begin{proof}
Sia $\xi \geq a$.
Per l'assioma~$T$ si ha che $a-x$ è standard.
Inoltre, per ogni $\varepsilon >0$ standard, risulta
\[
x - \varepsilon  < \xi  < x + \varepsilon \,,
\]
da cui $a-\varepsilon \leq \xi -\varepsilon <x$.
Poiché
\[
x > a - \varepsilon 
\qquad
\text{per ogni $\varepsilon >0$ con $\varepsilon$ standard}\,,
\]
deve essere $x\geq a$, altrimenti sarebbe possibile scegliere
$\varepsilon=a-x$ e dedurre che $x>x$.
\par
Il caso $\xi\leq a$ si tratta in modo simile.
\end{proof}
%
\begin{definizione}
\label{defn:grr}
Sia $\xi \in\R$.
Diciamo che $\xi$ è {\em infinitamente grande}, 
se si ha $|\xi| > M$ per ogni $M \in\R$ con $M$ standard.
\end{definizione}
%
\begin{theorem}
\label{thm:bw}
Sia $\xi$ un numero reale non infinitamente grande.
Allora esiste un numero reale standard $x$ tale che $\xi \simeq x$.
\end{theorem}
\begin{proof}
Sia $M\in\R$ con $M$ standard tale che $|\xi| \leq M$.
Poniamo
\[
A \,=\, ^{S}\!\left\{t \in \R|~ t \leq  \xi \right\} \,,\qquad
B \,=\, ^{S}\!\left\{t \in \R|~ t \geq  \xi \right\} \,.
\]
Dal momento che $M$ è standard, risulta $M\in B$.
Anche $-M$ è standard per l'Assioma T, per cui $-M\in A$.
Ne segue che $A$ e $B$ non sono vuoti.
\par
La frase aperta classica
\[
(s \in  A \quad \hbox{e} \quad t \in  B) \,\Longrightarrow\,  
s \leq  t
\,,
\]
dipendente dai parametri standard $A$, $B$ e $\leq$, 
è vera per ogni $s$, $t$ standard.
Per l'assioma~$T$, la proprietà è vera per ogni $s$ e $t$.
\par
Per il principio di Dedekind, esiste  un   elemento separatore 
fra $A$ e $B$.
Per l'assioma~$T$, esiste un elemento separatore $x$ standard.
\par
Dimostriamo che $\xi \simeq x$.
Per ogni $\varepsilon >0$ standard si ha
$x+\varepsilon > x$, per cui
$(x+\varepsilon )\not\in A$.
Poiché $(x+\varepsilon )$ è standard, ne  segue 
$\xi <x+\varepsilon$.
Analogamente si ha $(x-\varepsilon )\not\in B$, quindi
$x-\varepsilon <\xi$.
Pertanto risulta $|\xi -x| <\varepsilon$ per ogni 
$\varepsilon >0$ standard.
\end{proof}
%
\begin{proposizione}
\label{prop:cont}
Siano $f:\dom{f} \to\R$ una funzione standard, 
con $\dom{f}\subseteq \R$, e~$x$ un elemento standard di 
$\dom{f}$.
\par
Allora sono fatti equivalenti:
\begin{itemize}
\item[$(a)$]
$f$ è continua in $x$;
\item[$(b)$]
per ogni $\xi\in \dom{f}$ si ha
\[
\xi  \simeq  x \,\Longrightarrow\,  f(\xi) \simeq  f(x) \,.
\]
\end{itemize}
\end{proposizione}
\begin{proof}
\par
\par\noindent
$(a)\,\Longrightarrow\,(b)$~Sia $\xi \in \dom{f}$ con 
$\xi \simeq x$ e sia $\varepsilon>0$ standard.
Dalla $(a)$ sappiamo che esiste $\delta$ tale che
\[
\delta>0 \quad\hbox{e}\quad
(\forall \eta \in \dom{f}|~
|\eta-x|<\delta \,\Longrightarrow\,|f(\eta)-f(x)|<\varepsilon) \,.
\]
Si tratta di una frase aperta classica contenente i parametri
standard $\R$ (con la sua struttura), $\dom{f}$, $x$, $f$ ed 
$\varepsilon$.
Per l'assioma $T$ esiste allora un $\delta>0$ standard con
tale proprietà.
Poiché $\xi\simeq x$, risulta $|\xi-x|<\delta$, quindi
$|f(\xi)-f(x)|<\varepsilon$.
Pertanto si ha $f(\xi) \simeq f(x)$.
\par\noindent
$(b)\,\Longrightarrow\,(a)$~Dobbiamo dimostrare che
per ogni $\varepsilon$
\[
\varepsilon>0 \,\Longrightarrow\,
(\exists \delta>0,\,\forall \eta \in \dom{f}|~
|\eta-x|<\delta \,\Longrightarrow\,|f(\eta)-f(x)|<\varepsilon) \,.
\]
Questa volta si tratta di una frase aperta classica contenente i 
parametri standard $\R$, $\dom{f}$, $x$ e $f$.
Sempre per l'assioma $T$, è sufficiente dimostrare tale 
proprietà quando $\varepsilon$ è standard.
Sia quindi $\varepsilon$ standard con $\varepsilon>0$.
Per il Teorema~\ref{thm:simeqo} esiste $\hat{\delta}>0$ con 
$\hat{\delta} \simeq 0$.
Allora, se $\eta \in \dom{f}$ ed $|\eta-x| < \hat{\delta}$, 
si ha $\eta \simeq x$.
Ne segue $f(\eta) \simeq f(x)$, quindi $|f(\eta)-f(x)|<\varepsilon$,
dal momento che $\varepsilon$ è standard.
\end{proof}
%
Il fatto che la validità della proposizione precedente sia limitata al 
caso in cui $f$ e $x$ sono standard può lasciare un po' delusi.
Tuttavia l'informazione fornita è sufficiente, perché
tale risultato viene sempre utilizzato in combinazione con
l'assioma $T$, che ha proprio lo scopo di consentire di ridursi,
quando è lecito, a questo caso particolare.
%
\begin{theorem}
\label{thm:w}
\em (di Weierstrass)\em \
Sia $f:[a,b]\to \R$  una funzione continua con $a\leq b$.
Allora $f$ ammette massimo e  minimo.
\end{theorem}
\begin{proof}
L'affermazione ha la struttura
\[
\forall f|~\mathcal{P}(f,\R) \Longrightarrow \mathcal{Q}(f,\R)
\]
in cui $\mathcal{P}(f,\R)$ e $\mathcal{Q}(f,\R)$ sono frasi aperte
classiche e $\R$ (con tutta la sua struttura) funge da parametro
standard.
Per l'assioma $T$, basta  quindi trattare il caso in cui $f$ 
è standard.
Ne segue che $[a,b]=\dom{f}$ è standard, per cui sono
standard $a$ e $b$.
\par
Sia $F$ un sottoinsieme finito di $[a,b]$  contenente  tutti
gli elementi standard di $[a,b]$.
In particolare $a,b\in F$, per cui $F\neq\emptyset$.
Sia $\xi \in F$  un  punto  di massimo  per $f$  nell'ambito  
dei punti di $F$.
Risulta $|\xi| \leq |a| + |b|$ con $|a|+|b|$ standard.
Pertanto $\xi$ non è infinitamente grande.
Per il  Teorema~\ref{thm:bw}, esiste un numero reale 
standard $x_M$ tale che $\xi \simeq x_M$.
Per la Proposizione~\ref{prop:dis}, risulta $a\leq x_M\leq b$.
\par
Sia $x$ standard in $[a,b]$.
Risulta $x\in F$, quindi $f(\xi)\geq f(x)$.
D'altronde  dalla  continuità  di $f$  si deduce che
$f(\xi)\simeq f(x_M)$.
Per l'assioma $T$, $f(x_M)$ e $f(x)$ sono standard.
Dalla Proposizione~\ref{prop:dis} segue che $f(x_M)\geq f(x)$.
\par
Pertanto la frase aperta classica
\[
x \in  [a,b] \,\Longrightarrow\,  f(x_M) \geq  f(x) \,,
\]
dipendente dai parametri standard $a$, $b$, $f$ e $x_M$,
è vera per ogni $x$ standard.
Per l'assioma~$T$, essa è  vera per ogni $x$, ossia $x_M$ 
è un punto di massimo per~$f$.
\par
In modo simile si prova che $f$ ammette minimo.
\end{proof}


%--------------------------------------------------------------------

%\begin{thebibliography}{99}
%
\begin{thebibliography}{10}
\phantomsection
\addcontentsline{toc}{section}{Bibliografia}

\bibitem{nelson}
\textsc{E.~Nelson},
{Internal set theory: a new approach to nonstandard analysis},
\textit{Bull. Amer. Math. Soc.} \textbf{83} (1977), \textit{no.} 6,
1165--1198.
\bibitem{mdrobinson}
\textsc{A.~Robinson},
``Non-standard analysis'',
North-Holland Publishing Co., Amsterdam, 1966.
%
\end{thebibliography}
%



