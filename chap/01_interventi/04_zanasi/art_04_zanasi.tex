% !TEX encoding = UTF-8

\def \testauthor{Roberto Zanasi}

\makechapter
{\testauthor\astfootnote{\,ITIS Fermi, Modena}}%autore
{Non tutti gli infiniti sono uguali}%titolo
{Non tutti gli infiniti sono uguali\\
\emph{\testauthor}}%tittoc
{Non tutti gli infiniti sono uguali}%tittes
{La nozione di indistinguibilità tra due numeri iperreali, gemella della 
nozione di asintoticità dell'analisi standard, spesso non viene insegnata 
nelle 
scuole superiori per evitare di fornire agli studenti nuove occasioni per 
commettere errori. Vorrei analizzare alcuni errori frequenti che si possono 
presentare, soprattutto nell'uso degli infiniti indistinguibili, e proporre 
qualche aiuto, anche visuale, per lo studente in difficoltà.
}%sommario

% \documentclass[a4,12pt]{article}
% \usepackage{hyperref}
% \usepackage{mathtools}
% \usepackage[utf8]{inputenc}
% \usepackage[italian]{babel}
% \usepackage{lmodern}
% \usepackage[T1]{fontenc}
% \usepackage{pgf,tikz}
% %\usepackage{graphicx}
% %\usepackage{fixcmex} %serve per i simboli matematici che altrimenti non vengono 
% ingranditi
% \usepackage{pgfplots}
% 
% \title{Non tutti gli infiniti sono uguali}

% \newcommand\R{\mathbf{R}}
% %\newcommand\st[1]{\mathrm{st}\left( #1 \right)}
% \DeclareMathOperator{\st}{st}

%\begin{document}

\section{La definizione di numeri indistinguibili}

L'idea che sta alla base della definizione di numeri indistinguibili richiama 
la definizione di errore assoluto e di errore relativo che si studia in 
fisica. 
Se parliamo di segmenti, in modo da lasciar fuori per un momento dallo studio 
i numeri negativi, cosa intendiamo quando affermiamo che la differenza tra le 
loro misure è pari a un centimetro? 
Stiamo parlando di una differenza \emph{grande} o \emph{piccola}?

La risposta è che dipende da quanto sono lunghi i segmenti: ciò che conta è 
il rapporto tra il centimetro e la lunghezza totale del segmento, quello che 
in fisica si chiama errore relativo.

La definizione di segmenti indistinguibili ricalca questa idea: due segmenti 
si dicono indistinguibili se, nella scala in cui io riesco a vederli nella 
loro interezza, mi sembrano sovrapposti. 
Questa naturalmente non è una definizione matematica, ma ci dà l'idea di ciò 
che vogliamo dire.

Ecco, quindi, la definizione rigorosa: due numeri iperreali $a$ e $b$ 
\emph{non nulli} sono indistinguibili ($a\sim b$) se vale una delle seguenti 
relazioni 
(tutte equivalenti tra loro):
\begin{itemize}
\item $\dfrac{a}{b}\approx 1$,
\item $\st\left(\dfrac{a}{b}\right)=1$,
\item $\dfrac{a-b}{a}\approx 0$,
\item $\dfrac{a-b}{b}\approx 0$.
\end{itemize}
Due numeri sono dunque indistinguibili se il loro rapporto è infinitamente 
vicino a 1 o, alternativamente, se la loro differenza è infinitesima rispetto 
a entrambi.

Si può vedere facilmente che le quattro relazioni sono equivalenti. Infatti, 
se $\dfrac{a-b}{b}\approx 0$, possiamo dire che $\dfrac{a}{b}\approx 1$, e 
quindi anche che $\dfrac{b}{a}\approx 1$, e cioè che $1-\dfrac{b}{a}\approx0$, 
e infine che $\dfrac{a-b}{a}\approx0$.

Notiamo subito che la relazione di indistinguibilità non è definita per lo 
zero: se questa idea non viene ben assimilata, può produrre errori. 
Vedremo in seguito quali.

Osserviamo anche che questa relazione è di equivalenza: ogni numero diverso 
da zero è indistinguibile da sé stesso, dato che il rapporto è identicamente 
uguale a 1;
se $a\sim b$ allora $b\sim a$, dato che il loro rapporto è infinitamente 
vicino a 1 in qualunque modo si scelgano numeratore e denominatore; 
e infine vale la proprietà transitiva, perché se $a\sim b$ e $b\sim c$ allora 
$a \sim c$, visto che $\dfrac{a}{c}=\dfrac{a}{b}\cdot\dfrac{b}{c}$.

\section{Esempi di numeri indistinguibili}

Vediamo alcuni esempi in cui si applica la definizione. I due numeri $\delta$ e 
$\delta+2\delta^2$ (dove con $\delta$ si è indicato, come si fa di solito, un
infinitesimo) sono indistinguibili, perché
\[
\frac{\delta}{\delta+2\delta^2}=\frac{\delta}{\delta(1+2\delta)}=
\frac{1}{1+2\delta}\approx 1.
\]

Da questo esempio possiamo dedurre una regola generale: due infinitesimi non 
nulli sono indistinguibili se differiscono per un infinitesimo di ordine 
superiore.

Passiamo a un esempio che ha a che fare con numeri finiti non infinitesimi:
$5$ e $5+\delta$ sono indistinguibili, perché
\[
\frac{5}{5+\delta}\approx 1.
\]

Da questo secondo esempio possiamo dedurre questa regola generale: 
due numeri finiti non infinitesimi sono indistinguibili se differiscono per un 
infinitesimo, cioè se sono infinitamente vicini. Nel caso dei numeri finiti non 
infinitesimi l'indistinguibilità coincide con
l'essere infinitamente vicini.

Come ultimo esempio, esaminiamo due infiniti:
$H^2$ e $H^2+2H$ (dove con $H$ si è indicato, come si fa di solito, un infinito) 
sono indistinguibili, perché
\[
\frac{H^2}{H^2+2H}=\frac{H^2}{H^2\left(1+\frac{2}{H}\right)}=\frac{1}{1+\frac{2}
{H}}\approx 1.
\]

In generale, due numeri infiniti sono indistinguibili se differiscono per un 
infinito di ordine inferiore (e anche, quindi, se differiscono per un numero 
finito o per un infinitesimo).

Riassumendo, possiamo dire che:
\begin{itemize}
\item Nella somma di due infinitesimi si può trascurare un infinitesimo di 
ordine superiore;
\item nella somma di un finito non infinitesimo con un infinitesimo si può 
trascurare l'infinitesimo;
\item nella somma di un infinito con un infinitesimo si può trascurare 
l'infinitesimo;
\item nella somma di un infinito con un finito non infinitesimo si può 
trascurare il finito non infinitesimo;
\item nella somma di due infiniti si può trascurare un infinito di ordine 
inferiore.
\end{itemize}

Ma \emph{trascurare} a che scopo? A cosa servono questi numeri indistinguibili? 
Perché sono utili? 
La risposta sarà chiara dopo averne studiato alcune proprietà.

\section{Proprietà dei numeri indistinguibili}
Vale il seguente teorema: se $a\sim a'$ e $b\sim b'$ allora $ab\sim a'b'$ e 
$\dfrac{a}{b} \sim \dfrac{a'}{b'}$.

Vediamo la dimostrazione. L'ipotesi $a\sim a'$ e $b\sim b'$ ci dice che 
$\dfrac{a}{a'}\approx1$ e $\dfrac{b}{b'}\approx1$. Per dimostrare che
$ab\sim a'b'$ basta calcolare il rapporto $\dfrac{ab}{a'b'}$ e vedere che 
risulta infinitamente vicino a 1. E infatti:
\[
\dfrac{ab}{a'b'}=\dfrac{a}{a'}\cdot\dfrac{b}{b'}\approx1\cdot1=1.
\]

Per dimostrare invece che $a/b\sim a'/b'$ basta calcolare $\dfrac{a/b}{a'/b'}$:
\[
\dfrac{a/b}{a'/b'}=\dfrac{a}{a'}\cdot\dfrac{b'}{b}\approx1\cdot1=1.
\]

Anche in questo caso il risultato è infinitamente vicino a 1, e il teorema è 
dimostrato.

Sono quindi leciti passaggi come questi, con i numeri infiniti:
\[
\frac{2H^2-3H+1}{H^2+\sqrt{H}} \sim \frac{2H^2}{H^2} = 2.
\]

Oppure come questi, che hanno a che fare con numeri finiti non infinitesimi:
\[
\frac{9+2\delta}{3-\delta^2} \sim \frac{9}{3} = 3.
\]


O ancora come questi, con gli infinitesimi:
\[
\frac{\delta+2\delta^2}{\delta-\delta^3} \sim \frac{\delta}{\delta} = 1.
\]

\section{Errori}
Qual è il problema, quindi, nell'uso degli indistinguibili? Abbiamo visto che si 
``comportano bene'' con moltiplicazione e divisioni, ci piacerebbe che 
facessero altrettanto con somme e sottrazioni, ma non è così.

Se $a\sim a'$ e $b\sim b'$ non è sempre vero che 
$a+b \sim a'+b'$, né che $a-b \sim a'-b'$, e nemmeno che $f(a)\sim f(a')$, per 
ogni funzione $f$.

Per esempio, se $M$ è un infinito allora i due numeri $M^2$ e $M^2+3$ sono 
indistinguibili, ma la loro differenza
$(M^2+3) - M^2=3$ non è indistinguibile da 0, e $2^{M^2+3}$ non è 
indistinguibile da $2^{M^2}$,
infatti il loro rapporto non è infinitamente vicino a 1:
\[
\dfrac{2^{M^2+3}}{2^{M^2}}=2^3=8.
\]

Vediamo un altro esempio, che prende spunto dalla definizione del numero $e$:
\[
	\lim_{n\to +\infty}\left(1+\frac{1}{n}\right)^n=e,
\]
che, nel linguaggio dell'analisi non standard, indicheremmo in questo modo:
\[
	\st\left(1+\frac{1}{N}\right)^N=e,\quad\text{per ogni $N$ infinito}.
\]
Questo significa che
\[
	\st\left(\frac{\left(1+\dfrac{1}{N}\right)^N}{e}\right)=1,
\]
e sembra quasi naturale affermare che anche questo numero
\[
	\frac{\left(1+\dfrac{1}{N}\right)^{N^2}}{e^N}
\]
abbia parte standard uguale a 1, ma non è così. In realtà il valore corretto è
$1/\sqrt{e}$, perché anche in questo caso è nascosta una differenza.

Infatti è vero che 
\[
N^2\ln\left(1+\dfrac{1}{N}\right)\sim N^2\dfrac{1}{N}=N, \quad\text{se $N$ è 
infinito},
\]
ma non è vero che 
\[N^2\ln\left(1+\dfrac{1}{N}\right)-N\sim N^2\dfrac{1}{N}-N=N-N=0.
\]
Non ha senso parlare di indistinguibilità da zero. Usando gli indistinguibili in 
questo caso si conserva soltanto l'infinito di ordine superiore, che è $N$, ma 
questo viene
eliminato dalla sottrazione. Ci interesserebbe quindi sapere ``cosa c'è dopo'', 
cioè se in quella differenza sono presenti solo infinitesimi, o se invece 
possiamo trovare anche
numeri finiti non infinitesimi o, addirittura, infiniti di ordine inferiore, ma 
questo non lo possiamo sapere se seguiamo questa strada.

Per trovare il risultato corretto serve conoscere qualcosa di più: il polinomio 
di Taylor del logaritmo, o il teorema di De L'H\^{o}pital:

\begin{align*}
N^2\ln\left(1+\dfrac{1}{N}\right)-N &= 
N^2\left(\frac{1}{N}-\frac{1}{2N^2}+o\left(\frac{1}{N^2}\right)\right)-N\\
&\approx N - \frac{1}{2} - N\\
&= -\frac{1}{2}.
\end{align*}
La differenza tra i due infiniti è dunque un numero finito non infinitesimo, e 
non zero.


\section{Il calcolo degli asintoti obliqui}

Anche il calcolo di un asintoto obliquo di una funzione può essere insidioso. 
Dato $H$ infinito, si calcola il coefficiente angolare dell'eventuale asintoto 
obliquo in questo modo:
\[
m = \st\left(\frac{f(H)}{H}\right).
\]
E cioè
\[
\st\left(\frac{f(H)}{mH}\right)=1.
\]
Ciò significa che $f(H)$ è indistinguibile da $mH$, e fino a questo punto va 
tutto bene. Quando però dobbiamo calcolare
l'intercetta all'origine, in questo modo:
\[
q = \st(f(H)-mH),
\]
siamo di fronte a una differenza tra due infiniti indistinguibili.

Per esempio, se vogliamo calcolare
l'asintoto obliquo di 
\[
y=\dfrac{x^2}{x+1},
\]
troviamo il coefficiente angolare secondo questo calcolo:
\[
\dfrac{f(H)}{H}=\dfrac{H^2}{H(H+1)}\sim\dfrac{H^2}{H^2}=1	=m.
\]
Mentre un tipico errore nel calcolo di $q$ è il seguente:
\[
f(H)-mH=\dfrac{H^2}{H+1}-H\sim H-H = 0,
\]
calcolo sbagliato perché si è trovata una indistinguibilità da 0, che è 
impossibile.

Il calcolo corretto è invece il seguente:
\begin{align*}
f(H)-mH&=\dfrac{H^2}{H+1}-H\\
&=\dfrac{H^2-H^2-H}{H+1}\\
&=-\dfrac{H}{H+1}\\
&\sim-\dfrac{H}{H}\\
&=-1,
\end{align*}
che ci mostra perché il calcolo con gli indistinguibili non funziona: nel 
secondo passaggio si nota la differenza tra 
gli infiniti di grado massimo (e cioè $H^2-H^2$), e si vede che, usando gli 
indistinguibili, perdiamo il termine che ci permette 
di trovare il risultato corretto, $-H$, un infinito di ordine inferiore rispetto 
a $H^2$.

Insomma, quando calcoliamo $m=\st\left(\dfrac{f(H)}{H}\right)$, stiamo cercando 
quel numero reale $m$ infinitamente vicino a $\dfrac{f(H)}{H}$. 
Questo significa che
\[
m = \dfrac{f(H)}{H} + \varepsilon,
\]
dove $\varepsilon$ è un infinitesimo. Utilizzando gli strumenti ottici 
dell'analisi non standard possiamo dire che,
facendo uno zoom infinito all'indietro, vediamo che la curva è indistinguibile 
da una retta che, necessariamente, passa per l'origine.
È possibile però che ci sia una differenza di quota tra l'asintoto e la retta 
$y=mx$, che non vediamo, come nella seguente figura,
che mostra la curva e l'asintoto, viste da molto lontano.

\begin{tikzpicture}[domain=0:4]

\draw[->] (-0.2,0) -- (4.2,0) node[right] {$x$};
\draw[->] (0,-1.2) -- (0,4.2) node[above] {$f(x)$};
	\draw[color=blue,thick] plot (\x,{(15*\x * \x /(15*\x +1)}) ;%node[right] 
;%{$f(x)$};
	\draw[color=red] plot (\x,{\x-1/15});% node[right] ;%{$f(x) =x-1$};
\end{tikzpicture}

Viste da lontano, tutte le rette sembrano passare per l'origine.

L'uguaglianza
\[
m = \dfrac{f(H)}{H} + \varepsilon
\]
significa che
\[
mH = f(H) + \varepsilon H
\]
e, quindi, che
\[
f(H)-mH = -\varepsilon H.
\]


Non siamo autorizzati a dire che $-\varepsilon H = 0$, perché il prodotto tra un 
infinitesimo e un infinito è una forma indeterminata. Dunque, quando calcoliamo
l'intercetta all'origine di un asintoto obliquo, ci troviamo di fronte al 
calcolo di $f(H)-mH$, che è \emph{sempre una forma indeterminata}.

\section{Il lancio di una moneta}

Vediamo un esempio di tipo completamente diverso, in cui ancora una volta si può 
incontrare la differenza tra due infiniti indistinguibili: il lancio di una 
moneta.

Se si lancia una moneta non truccata, la probabilità che esca \emph{testa} è 
$1/2$, uguale alla probabilità che esca \emph{croce}.

Se si esegue il lancio molte volte, ci si aspetta che il rapporto tra numero di 
teste e numero di croci si avvicini 
sempre di più a 1. Detto in termini più rigorosi: secondo la legge dei grandi 
numeri il rapporto tra numero di teste e numero di croci converge in probabilità 
a 1.

Analizziamo una serie di lanci e scopriamo che \emph{testa} è uscito un milione 
di volte. Possiamo affermare che il numero di \emph{croci} 
è molto vicino a un milione? La risposta corretta è, come spesso succede in 
matematica, \emph{dipende}.
Quanto è \emph{molto vicino}?
Quale risultato ci si aspetta? Quanto può essere grande la differenza tra numero 
di \emph{teste} e numero di \emph{croci}
se il numero di \emph{teste} è uguale a un milione? La risposta è
poco meno di 1500.

Ma c'è di più: all'aumentare del numero di lanci questa differenza aumenta, non 
diminuisce, come forse ci si aspetterebbe.
Se vediamo che ci sono 1500 teste in più e deduciamo che le croci sono 
\emph{ritardatarie}, e che certamente prima o poi 
ne usciranno molte, sbagliamo clamorosamente:
la moneta non ha memoria.

Vediamo ora come introdurre il calcolo con gli infiniti.


Se $T$ e $C$ sono due infiniti che rappresentano il numero di \emph{teste} e il 
numero di \emph{croci}, il fatto che
essi siano indistinguibili (perché $T/C\approx1$ con probabilità 1), non implica 
che la loro differenza sia infinitesima.

Indichiamo con $D_n$ la differenza tra numero di \emph{teste} e numero di 
\emph{croci} dopo $n$ lanci:
ogni volta che $n$ aumenta di 1, il valore di $D_n$ aumenta o diminuisce di 1. 
Possiamo quindi scrivere la relazione
\[
D_{n+1}=D_n\pm1.
\]
Eleviamo al quadrato a destra e a sinistra e otteniamo:
\[
D_{n+1}^2=D_n^2+1\pm2D_n.
\]
Ora calcoliamo il valor medio:
si immagina di fare tutte le sequenze di $n$ lanci, si sommano tutti i valori di 
$D$ così ottenuti, e si divide per il numero di sequenze.
Il valor medio di $D_n$ è zero, perché per ogni sequenza di \emph{teste} e 
\emph{croci}, esiste la sequenza gemella che si ottiene 
scambiando tra loro \emph{teste} e \emph{croci}. Quindi per ogni $D_n$ positivo 
esiste un $D_n$ negativo che annulla la somma.
Concludiamo quindi che il valor medio di $D_{n+1}^2$ è uguale al valor medio di 
$D_n^2+1$.

Dunque il valor medio del quadrato di $D_n$ aumenta di 1 a ogni lancio, e quindi 
dopo $n$ lanci vale esattamente $n$.

La radice del valor medio di $D_n^2$ (che è lo scarto quadratico medio) è uguale 
a $\sqrt{n}$.

Se $T$ e $C$ sono due infiniti che rappresentano il numero di \emph{teste} e il 
numero di \emph{croci} dopo infiniti lanci, 
essi sono indistinguibili (con probabilità 1), ma la loro differenza è un 
infinito di ordine inferiore.

L'immagine seguente mostra una simulazione al computer di un lancio di 50000 
monete.

\includegraphics[width=\textwidth]{testaocroce2_mono.png}

Quella che sembra l'asse delle ascisse è, in realtà, il rapporto tra 
numero di \emph{teste} e numero di \emph{croci}:
se ingrandissimo intorno all'origine il grafico, vedremmo qualche piccola
oscillazione che si smorza sempre di più.
La spezzata, invece, rappresenta la differenza tra \emph{teste} e
\emph{croci}: si nota che le sue oscillazioni sono sempre più ampie, e 
arrivano tranquillamente a toccare e superare la curva di equazione 
$y=\pm\sqrt{x}$.

Terminiamo questa parte con un aneddoto finale: Enrico Fermi, dopo avere 
perso una partita a tennis a Los Alamos per 6-4 contro un matematico, 
gli disse che l'altro non poteva dire di avere vinto perché lo scarto era 
inferiore alla radice di $n$, con $n = 6+4 = 10$.


\section{Una versione debole della formula di Stirling}
% TODO 11/06 Zanasi chiede di ombreggiare i rettangoli
Concludiamo gli esempi relativi agli infiniti indistinguibili cercando di 
ricavare una versione debole della formula
di Stirling. Una formula, cioè, che ci darà un'espressione indistinguibile per 
$\ln(n!)$ (mentre la versione
più nota fornisce un'espressione per $n!$).

Partiamo dal grafico seguente:

\begin{tikzpicture}
\begin{axis}[
    width=10cm,
 domain=0:5, samples=200, 
 xlabel={$x$}, ylabel={$y(x)$},
    %grid
]
\addplot[red,smooth,very thick]    {ln(x)};
%\addplot[smooth,dashed] {+(1+2*x)*exp(-2*x)};
%\addplot[smooth,dashed] {-(1+2*x)*exp(-2*x)};
\addplot [blue, domain=-0.3:5] {0};
\addplot [blue] coordinates {(0,-4) (0,3)};

\addplot [orange] coordinates {(1,0)  (1,ln(2)};
\addplot [orange] coordinates {(2,0)  (2,ln(3)};
\addplot [orange] coordinates {(3,0)  (3,ln(4)};
\addplot [orange] coordinates {(4,0)  (4,ln(4)};

\addplot [orange] coordinates {(1,ln(2) (2,ln(2)};
\addplot [orange] coordinates {(2,ln(3) (3,ln(3)};
\addplot [orange] coordinates {(3,ln(4) (4,ln(4)};


\end{axis}
% Retinatura rettangoli
\def \xo{2.43}
\def \yo{3.95}
\def \larg{1.32}
\fill [fill=gray, opacity=.3] 
      (\xo, \yo) rectangle (\xo+\larg, 4.53)
      (\xo+\larg, \yo) rectangle (\xo+2*\larg, 4.87)
      (\xo+2*\larg, \yo) rectangle (\xo+3*\larg, 5.1);
\end{tikzpicture}

in cui si osservano la funzione $y=\ln x$ e tre rettangoli aventi base unitaria, 
e altezze uguali a $\ln 2$, $\ln 3$ e $\ln 4$. Le loro aree
saranno, dunque, proprio $\ln 2$, $\ln 3$ e $\ln 4$. Tutti i rettangoli hanno 
una parte che si trova sopra la curva, e quindi possiamo dire
che la somma delle loro aree supera l'area sottesa dalla funzione logaritmo 
nell'intervallo $[1,4]$. In formule:
\[
\sum_{k=1}^4 \ln k = \ln1+\ln2+\ln3+\ln4 >\int_1^4\ln x dx,
\]
dato che $\ln 1 = 0$.

Possiamo poi generalizzare in modo da non fermare il calcolo a 4, ma all'intero 
$n$:
\begin{align*}
\sum_{k=1}^n \ln k &> \int_1^n \ln x dx, \\
&> \left[x\ln x\right]_1^n - \int_1^n dx,\\
&> n\ln n -1\ln 1 -\left[x\right]_1^n,\\
&>n\ln n -n +1.
\end{align*}


Ora osserviamo questa nuova figura:

\begin{tikzpicture}
\begin{axis}[
    width=10cm,
 domain=0:5, samples=200, 
 xlabel={$x$}, ylabel={$y(x)$},
    %grid
]
\addplot[red,smooth,very thick]    {ln(x)};
%\addplot[smooth,dashed] {+(1+2*x)*exp(-2*x)};
%\addplot[smooth,dashed] {-(1+2*x)*exp(-2*x)};
\addplot [blue, domain=-0.3:5] {0};
\addplot [blue] coordinates {(0,-4) (0,3)};

\addplot [orange] coordinates {(2,0)  (2,ln(2)};
\addplot [orange] coordinates {(3,0)  (3,ln(3)};
\addplot [orange] coordinates {(4,0)  (4,ln(4)};
\addplot [orange] coordinates {(5,0)  (5,ln(4)};

\addplot [orange] coordinates {(2,ln(2) (3,ln(2)};
\addplot [orange] coordinates {(3,ln(3) (4,ln(3)};
\addplot [orange] coordinates {(4,ln(4) (5,ln(4)};
\end{axis}
% Retinatura rettangoli
\def \xo{2.43}
\def \yo{3.95}
\def \larg{1.32}
\fill [fill=gray, opacity=.3] 
      (\xo+\larg, \yo) rectangle (\xo+2*\larg, 4.53)
      (\xo+2*\larg, \yo) rectangle (\xo+3*\larg, 4.87)
      (\xo+3*\larg, \yo) rectangle (\xo+4*\larg, 5.1);
\end{tikzpicture}

in cui sono presenti gli stessi rettangoli della figura precedente, però 
spostati verso destra di un'unità. In questo caso i rettangoli
sono tutti al di sotto della curva, ma si estendono fino al punto di ascissa 5. 
Possiamo allora scrivere che
la somma $\ln2+\ln3+\ln4$ è minore dell'area sottesa dal logaritmo
nell'intervallo $[1,5]$. Cioè
\[
\sum_{k=1}^4 \ln k = \ln1+\ln2+\ln3+\ln4 <\int_1^5\ln x dx,
\]
dato che $\ln 1 = 0$.


Generalizzando, possiamo scrivere:
\begin{align*}
\sum_{k=1}^{n-1}\ln k &< \int_1^n\ln x dx,\\
&<n\ln n -n +1.
\end{align*}

Ora vorremmo che la sommatoria a sinistra del simbolo di disuguaglianza non si 
fermasse all'indice $n-1$, ma a $n$, come quella che abbiamo trovato
in precedenza. Sommiamo quindi $\ln n$ a destra e a sinistra:
\begin{align*}
\ln n+\sum_{k=1}^{n-1}\ln k&< n\ln n-n+1+\ln n,\\
\sum_{k=1}^{n}\ln k&< n\ln n-n+\ln n+1.
\end{align*}

Ora mettiamo insieme le due disuguaglianze trovate, dopo aver osservato che
\[
\ln 1 + \ln 2 + \dots + \ln n = \ln(n!).
\]
Otteniamo che:
\[
n\ln n -n + 1 < \ln(n!) < n\ln n - n + \ln n +1.
\]
Dividiamo a destra e a sinistra per $n\ln n -n$, che è positivo per $n\ge3$:
\[
\frac{n\ln n -n + 1}{n\ln n -n} < \frac{\ln(n!)}{n\ln n -n} < \frac{n\ln n - n + 
\ln n +1}{n\ln n -n},
\]
e, per il principio di trasferimento, passiamo a $N$ ipernaturale infinito:
\[
\frac{N\ln N -N + 1}{N\ln N -N} < \frac{\ln(N!)}{N\ln N -N} < \frac{N\ln N - N + 
\ln N +1}{N\ln N -N}.
\]
Abbiamo fatto il rapporto per poter spezzare le due frazioni agli estremi:
\[
1\approx 1+\frac{1}{N\ln N -N} < \frac{\ln(N!)}{N\ln N -N} < 1+\frac{ \ln N 
+1}{N\ln N -N}\approx 1,
\]
in modo da notare che esse sono entrambe infinitamente vicino a 1.
Possiamo finalmente scrivere il risultato che abbiamo ottenuto:
\[
\frac{\ln(N!)}{N\ln N -N} \approx 1,
\]
che ci dice che
\[
\ln(N!) \sim N\ln N -N.
\]

Abbiamo trovato un'espressione indistinguibile per il logaritmo del fattoriale 
di $N$, ma purtroppo non possiamo applicare
la funzione esponenziale a destra e a sinistra per ricavare un'espressione 
indistinguibile per $N!$: non esiste una proprietà
dei numeri indistinguibili che ci permetta di farlo. 

Sarebbe sbagliato affermare che $N!$ è indistinguibile da $e^{N\ln N - 
N}=N^N/e^N$. Il risultato corretto è il seguente:
\[
N! \sim \frac{N^N}{e^N}\sqrt{2\pi N},
\]
ma non so come trovare quella $\sqrt{2\pi}$ con passaggi adatti a studenti di 
scuola superiore, e quindi mi fermo qui.
% 
% \begin{thebibliography}{10}
% \phantomsection
% \addcontentsline{toc}{section}{Bibliografia}
% 
% \bibitem[AAVV 2019]{zambelli} 
% \textsc{Daniele Zambelli} (a cura di), 
% \emph{Matematica Dolce 5}, 
% \url{www.matematicadolce.eu}
% 
% \bibitem[Goldoni, 2016]{goldoni1} 
% \textsc{Giorgio Goldoni}, 
% \emph{I numeri Iperreali}, 
% \url{ilmiolibro.kataweb.it/libro/scienza-e-tecnica/106196/i-numeri-iperreali
% }
% 
% \bibitem[Henle, Kleinberg, 1979]{henle} 
% \textsc{James M. Henle, Eugene Kleinberg}, 
% \emph{Infinitesimal calculus}, 
% New York, 1979
% 
% \bibitem[Keisler, 2015]{keisler} 
% \textsc{Howard Jerome Keisler},
% \emph{Elementary Calculus: An Infinitesimal Approach}, 
% \url{www.math.wisc.edu/~keisler/calc.html}
% 
% \end{thebibliography}
% \end{document}
