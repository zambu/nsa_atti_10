% !TEX encoding = UTF-8

\def \testauthor{Daniele Zambelli}

\input{chap/01_interventi/05_zambelli/art_05_zambelli_grafici}

\makechapter
{\testauthor\astfootnote{\,Ha insegnato anche al Liceo Fracastoro di 
Verona}}% autore
{Limiti e infinito}% titolo
{Limiti e infinito\\
\emph{\testauthor}}% tittoc
{Limiti e infinito}% tittes
{I limiti sono l'argomento obbligato per affrontare l'analisi 
con il metodo standard. 
Tutte le importanti scoperte dell'analisi che noi riproponiamo a scuola sono 
state fatte prima dell'invenzione dei limiti.
Nell'Analisi Non Standard è possibile affrontare derivate e 
integrali senza usare i limiti, 
alcuni testi non standard li ignorano completamente, altri li introducono 
dopo aver trattato la derivazione e l'integrazione.

La semplicità del concetto di limite espresso in forma non standard, permette 
di proporlo presto nel percorso di studi come semplice applicazione del 
calcolo con i numeri iperreali.

In questo intervento, riprenderò brevemente il concetto di limite finito al 
finito (vedi \cite{zambelli1}) per approfondire poi i limiti infiniti e i 
limiti all'infinito \footnote
{\,Si può trovare un'esposizione organica con esempi e esercizi nel capitolo 
``Continuità e limiti'' del testo ``Matematica Dolce'' volume 5 
\cite{zambelli2}.
Nello stesso testo si trova anche l'esposizione dell'insieme dei numeri reali 
che è un prerequisito necessario.}.
}%sommario

% \makecapitolo
% {Daniele Zambelli - Verona}
% {Limiti e infinito}
% {I limiti sono l'argomento obbligato per affrontare l'analisi con il metodo 
% standard. 
% Tutte le importanti scoperte dell'analisi che noi riproponiamo a scuola sono 
% state fatte prima dell'invenzione dei limiti.
% Nell'Analisi Non Standard è possibile affrontare derivate e 
% integrali senza usare i limiti, 
% alcuni testi non standard li ignorano completamente, altri li introducono 
% dopo aver trattato la derivazione e l'integrazione.
% 
% La semplicità del concetto di limite espresso in forma non standard, permette 
% di proporlo presto nel percorso di studi come semplice applicazione del 
% calcolo con i numeri iperreali.
% 
% In questo intervento, riprenderò brevemente il concetto di limite finito al 
% finito (vedi \cite{zambelli1}) per approfondire poi i limiti infiniti e i 
% limiti all'infinito \footnote{
% Si può trovare un'esposizione organica con esempi e esercizi nel capitolo 
% ``Continuità e limiti'' del testo ``Matematica Dolce'' volume 5 
% \cite{zambelli2}.
% Nello stesso testo si trova anche l'esposizione dell'insieme dei numeri reali 
% che è un prerequisito necessario.}.
% }


\section{L'idea di limite}

I limiti studiano il \emph{comportamento} di una funzione 
quando l'argomento \emph{è vicino} ad \emph{un certo valore}. 
Non ci interessa cosa succede quando l'argomento della funzione ha proprio 
quel valore.
Infatti, quando non conosciamo il valore di una funzione in un punto vogliamo 
almeno conoscere come si comporta nelle sue vicinanze.
Questo comportamento può essere espresso con un numero reale o con un qualche 
altro simbolo.

In generale, quando l'argomento di una funzione è ``vicino'' ad un certo 
valore, la funzione potrebbe avere uno dei seguenti comportamenti:
\begin{enumerate} [noitemsep]
\item non è definita;
\item assume valori che non sono ``vicini'' tra di loro;
\item assume valori tutti ``vicini'' a un certo valore;
\item assume valori ``grandissimi'' in valore assoluto.
\end{enumerate}
Negli ultimi due casi possiamo parlare di \emph{limite}.
Ma per poter usare i limiti come strumento matematico dobbiamo precisare i 
termini vaghi usati in questa descrizione.

Nell'analisi standard possiamo pensare ai limiti come a un processo di 
avvicinamento dell'argomento ad un certo valore: 
``\(x\) che tende a \(c\)''.

\affiancati{.54}{.44}{
Processo che avviene con una gara al rilancio: una gara al tiro con 
l'arco in cui il giudice restringe continuamente il bersaglio.

\emph{RH}: se sono vicino a \(c\) colpisco il bersaglio;\\
\emph{SN}: ma io restringo il bersaglio attorno a \(L\);\\
\emph{RH}: se mi avvicino di più a \(c\) colpisco anche questo 
bersaglio;\\
\emph{SN}: ma io metto un bersaglio ancora più piccolo;\\
\emph{RH}: se mi avvicino abbastanza a \(c\) colpisco un bersaglio 
piccolo quanto vuoi.
}{
\hspace{-10mm}\scalebox{.8}{\limiteedfin}
}

Questo tipo di processo si descrive in matematica con una frase piuttosto 
complicata e dà origine alla definizione \(\epsilon\) - \(\delta\) 
di limite:

\begin{align*}
&\lim_{x \to c} f(x) = L \text{~se~} \\
&\quad \forall \epsilon > 0 \quad \exists \delta > 0 
\quad \forall x
\tonda{\tonda{x \neq c \sand \abs{x-c} < \delta} \srarrow 
\abs{f(x)-L}< \epsilon}
\end{align*}


\section{Limiti finiti al finito}

Mentre nell'Analisi Standard (SA) il limite sintetizza un 
\emph{processo infinito}, 
nell'Analisi Non Standard (NSA), il limite coglie il valore di una funzione 
in un generico punto \(x\) \emph{infinitamente vicino} a \(c\) e diverso da 
\(c\).

\affiancati{.45}{.53}{
\begin{definizione}
Il numero reale \(L\) è il \textbf{limite} della funzione \(f(x)\) 
per \(x\) che tende a un valore \(c\) se \(f(x)\) è infinitamente vicino a 
\(L\) per tutti gli \(x\) infinitamente vicini a \(c\) e diversi da 
\(c\).
\[\lim_{x \to c} f(x) = L \text{~~se~~} 
\forall x \approx c \text{~~e~~} x \neq c ~\srarrow~ f(x) \approx L\]
\end{definizione}
}{
\hspace{-5mm}\scalebox{.9}{\limitedef}
}

Dato che questo comportamento, dove possibile, è indicato da un
numero reale, il limite di una funzione, quando è finito, è legato
alla funzione parte standard:
\[\lim_{x \to c} f(x) = \pst{f(c + \epsilon)}\]
\vspace{-2.5em}
\begin{center} 
\emph{se questa parte standard esiste e è sempre la stessa 
per ogni infinitesimo \(\epsilon \neq 0\)}.
\end{center}

Questa definizione è \emph{operativa}: ci permette di \emph{calcolare} 
i limiti finiti al finito con un procedimento in due passi:
\begin{enumerate} [noitemsep]
\item  calcoliamo il valore iperreale: \(f(c + \epsilon)\);
\item se il valore ottenuto è finito, calcoliamo: \(\pst{f(c + \epsilon)}\).
\end{enumerate}

\vspace{1em}
\textbf{Primo esempio} 

Studia il comportamento della funzione \quad 
\(f(x) = \dfrac{x^2-4}{x^2+x-6}\) \quad 
vicino a \(+2\). 

\affiancati{.59}{.39}{
Per prima cosa calcoliamo il valore (iperreale) della funzione in un punto 
infinitamente vicino a \(+2\).
\begin{align*}
f(2 + \epsilon) &\stackrel{1}{=}
\frac
  {\tonda{2+\epsilon}^2-4}
  {\tonda{2+\epsilon}^2+\tonda{2+\epsilon}-6} \stackrel{2}{=}\\
&\stackrel{2}{=}
\frac
  {\cancel{+4}+4\epsilon+\epsilon^2~\cancel{-4}}
  {\cancel{+4}+4\epsilon+\epsilon^2~\cancel{+2}+\epsilon~\cancel{-6}}
\stackrel{3}{=}\\
&\stackrel{3}{=}
\frac
  {\cancel{\epsilon} \tonda{4+\epsilon}}
  {\cancel{\epsilon} \tonda{5+\epsilon}}
\end{align*}
}{
% \begin{center} 
\hspace{-6mm} \scalebox{.8}{\limitefesea} 
% \end{center}
}

\vspace{.5em}
Dato che il valore ottenuto è finito 
(il quoziente di due finiti non infinitesimi) 
calcoliamo la sua parte standard:
\begin{align*}
\pst{\frac{4+\epsilon} {5+\epsilon}} \stackrel{4}{=}
\frac{\pst{+4+\epsilon}}{\pst{+5+\epsilon}} \stackrel{5}{=} \frac{4}{5}
\end{align*}
Poiché il valore ottenuto è indipendente dall'infinitesimo 
\(\epsilon \neq 0\):
\(\displaystyle \lim_{x \to 2} f(x) = \dfrac{4}{5}\).

Motivazioni dei passaggi:
\begin{enumerate} [nosep]
\item calcoliamo \(f(2+\epsilon)\);
\item svolgendo i calcoli algebrici, otteniamo il rapporto tra due 
infinitesimi dato che tutti i termini non infinitesimi si annullano;
\item possiamo raccogliere e semplificare \(\epsilon\)
(che è diverso da zero);
\item la parte standard della frazione è uguale al rapporto delle parti 
standard del numeratore e del denominatore dato che il denominatore non è 
infinitesimo;
\item la parte standard di un reale più un infinitesimo è il reale stesso
e non dipende dall'infinitesimo \(\epsilon\).
\end{enumerate}

\bigskip
\emph{Osservazioni}
\begin{itemize} [noitemsep]
\item 
Il calcolo del limite richiede soltanto concetti già presentati trattando
i \emph{numeri iperreali}
\item 
A differenza della definizione standard, quella non standard 
fornisce un \emph{metodo per calcolare} il limite.
\end{itemize}

\subsection{Casi in cui la definizione fallisce}

La definizione permette di calcolare il limite 
finito solo se ci sono alcune condizioni, in alcuni casi può fallire:
\begin{enumerate} [nosep]
\item se la funzione \(f(c+\epsilon)\) non è definita per alcuni valori 
dell'infinitesimo \(\epsilon \neq 0\); 
\item se al variare di \(\epsilon\) la parte standard 
\(\pst{f(c+\epsilon)}\) non è sempre la stessa;
\item se \(f(c+\epsilon)\) è un numero infinito e quindi non ha parte 
standard. 
\end{enumerate}

Nei primi due casi \emph{non esiste il limite}, ma si può cercare se è 
presente una forma più debole di limite: 
il \emph{limite sinistro} o il \emph{limite destro}.

Il terzo caso può essere affrontato ampliando la definizione.

\section{Limiti infiniti al finito}

Possiamo ampliare la definizione non standard di limite in modo da includere 
anche i limiti infiniti. 
Per operare questo ampliamento utilizzeremo gli usuali simboli di infinito 
dando loro il seguente significato:
\begin{itemize}
\item \(-\infty\) per indicare un \emph{generico numero infinito negativo};
\item \(+\infty\) per indicare un \emph{generico numero infinito positivo}.
\end{itemize}
La nuova definizione contemplerà quindi anche i due casi:
\[\lim_{x \to c} f(x) = -\infty \quad 
\text{~~se~~} f(c+\epsilon) 
\text{~~è un infinito negativo per ogni infinitesimo~~} \epsilon \neq 0\]
\[\lim_{x \to c} f(x) = +\infty \quad 
\text{~~se~~} f(c+\epsilon) 
\text{~~è un infinito positivo per ogni infinitesimo~~} \epsilon \neq 0\]

Quindi la nuova procedura in due passi diventa: 

\vspace{.5em}
\textbf{Primo passo:}

% \vspace{.5em}
Calcoliamo il valore (iperreale) di \(f(c+\epsilon)\) 
% con \(\epsilon\) infinitesimo diverso da zero.
per un generico infinitesimo \(\epsilon \neq 0\).

% \renewcommand{\labelitemii}{$\diamond$}
% \renewcommand{\labelitemii}{$\triangleright$}
% \renewcommand{\labelitemii}{$\RHD$}
% \renewcommand{\labelitemi}{\(\triangleright\)}
\renewcommand{\labelitemii}{\(\circ\)}
% \renewcommand{\labelitemii}{\(\bullet\)}

\vspace{.5em}
\textbf{Secondo passo:}
\begin{itemize} [nosep]
\item 
se \(f(c+\epsilon)\) è \emph{finito} il limite sarà la sua parte standard se 
questa è indipendente da \(\epsilon\);
\item 
se \(f(c+\epsilon)\) è \emph{infinito} il limite sarà: 
  \begin{itemize} [nosep]
  \item \(+\infty\) se, per ogni infinitesimo 
    \(\epsilon \neq 0\), \(f(x + \epsilon)\) è un infinito positivo; 
  \item \(-\infty\) se, per ogni infinitesimo 
    \(\epsilon \neq 0\), \(f(x + \epsilon)\) è un infinito negativo. 
  \end{itemize}
\end{itemize}

% \newpage %---------------------------------------------
\vspace{1em}
\textbf{Secondo esempio} 

Studia il comportamento della funzione \quad 
\(f(x)=\dfrac{x+2}{x^2+10x+25}\) \quad 
vicino a \(-5\).

\affiancati{.59}{.39}{
Per prima cosa calcoliamo il valore (iperreale) della funzione in un punto 
infinitamente vicino a \(-5\).
\begin{align*}
f(-5 + \epsilon) &= 
  \dfrac{\tonda{-5 +\epsilon}+2}
        {\tonda{-5 +\epsilon}^2+10\tonda{-5 +\epsilon}+25} =\\ 
&=\dfrac{-5 +\epsilon +2}
        {\cancel{+25}~\cancel{-10\epsilon} +\epsilon^2~
         \cancel{-50}~\cancel{+10\epsilon}~\cancel{+25}} =\\ 
&=\dfrac{-3 +\epsilon}{\epsilon^2}
\end{align*}
Il valore ottenuto è un infinito perché è il rapporto tra un 
\emph{non infinitesimo} e un \emph{infinitesimo}.
% \[\lim_{x \to -4} \dfrac{x+2}{x^2+8x+16} = -\infty\]
}{
\hspace{-10mm} \scalebox{.7}\limiteiesea 
}

\vspace{.3em}
Dato che per qualunque valore di \(\epsilon\) il numeratore è negativo e 
il denominatore è positivo, il risultato è un infinito negativo: \quad 
\(\displaystyle \lim_{x \to -4} f(x) = -\infty\)
% \[\lim_{x \to -4} f(x) = -\infty\]

\vspace{1em}
\textbf{Terzo esempio} 

Studia il comportamento della funzione \quad 
\(f(x) = \dfrac{-2x+6}{x-4}\) \quad 
vicino a \(+4\). 

Calcoliamo il valore della funzione in un punto infinitamente vicino a 
\(+4\).
\[f(4 + \epsilon) = 
  \frac{-8 - 2 \epsilon + 6}{4 - \epsilon - 4} =
  \frac{-2 - 2 \epsilon}{\epsilon} \]
\(f(4 + \epsilon)\) è un numero infinito 
(\emph{fni} / \emph{inn} = \emph{I}) dovremo 
quindi usare la definizione di limite infinito.
Poiché al variare di \(\epsilon\) 
l'infinito che si ottiene può avere segno diverso:

\vspace{.5em}
\affiancati{.49}{.49}{
% il valore ottenuto non è sempre positivo e non è neppure sempre negativo, 
\begin{center} \emph{non esiste il limite}. \end{center}

Qualcosa comunque possiamo dire se ci accontentiamo dei limiti
parziali. 
Dato che l'infinito che otteniamo ha segno opposto al segno di \(\epsilon\), 
per valori di \(x\) infinitamente vicini a \(4\) e minori di \(4\) 
il valore della funzione è sempre un infinito positivo 
mentre per valori di \(x\) infinitamente vicini a \(4\) e maggiori di \(4\) 
la funzione ha sempre un valore infinito negativo. 
Quindi:
}{
\hspace{-15mm} \scalebox{.7}\limiteieseb 
}

\[\lim_{x \to 4^-} \frac{-2x+6}{x-4} = +\infty \qquad 
\lim_{x \to 4^+} \frac{-2x+6}{x-4} = -\infty\]


\section{Limiti all'infinito}

Finora abbiamo visto limiti per \(x\) che tende ad un valore finito, ma è 
interessante studiare anche i limiti 
quando \(x\) è un \emph{qualsiasi} valore infinito negativo o 
quando \(x\) è un \emph{qualsiasi} valore infinito positivo:
% \(x \to -\infty \text{~~o~~} x \to +\infty\):
\[\lim_{x \to -\infty} f(x) \qquad 
\lim_{x \to +\infty} f(x)\]
Il primo passo sarà studiare il valore della funzione (iperreale) in un 
generico infinito, negativo nel primo caso o positivo nel secondo:
\[f(M)\]
poi distingueremo i seguenti casi:
\begin{itemize} [left=0mm]
\item 
Se \(f(M)\) è un valore finito, calcoliamo \(\pst{f(M)}\).\\
Se questo valore esiste e è sempre lo stesso per ogni infinito negativo 
\(M\):
\[\lim_{x \to -\infty} f(x) = \pst{f(M)}\]
Se questo valore esiste e è sempre lo stesso per ogni infinito positivo 
\(M\):
\[\lim_{x \to +\infty} f(x) = \pst{f(M)}\]
\item 
Se \(f(M)\) è un valore infinito:
\vspace{-1em}
\begin{align*}
\lim_{x \to -\infty} f(x) = -\infty \quad 
\text{se~} f(M) 
\text{~è un \emph{infinito negativo} per ogni infinito~} M < 0\\
\lim_{x \to -\infty} f(x) = +\infty \quad 
\text{se~} f(M) 
\text{~è un \emph{infinito positivo}~ per ogni infinito~} M < 0\\
\lim_{x \to +\infty} f(x) = -\infty \quad 
\text{se~} f(M) 
\text{~è un \emph{infinito negativo} per ogni infinito~} M > 0\\
\lim_{x \to +\infty} f(x) = +\infty \quad 
\text{se~} f(M) 
\text{~è un \emph{infinito positivo}~ per ogni infinito~} M > 0\\
\end{align*}
\end{itemize}

\textbf{Quarto esempio}\\
Studia il comportamento della funzione \quad 
\(f(x) = \dfrac{3x^2-4x+5}{x^2-2x+3}\)\\
per un generico valore infinito negativo di \(x\) e 
per un generico valore infinito positivo di \(x\).
% per \(x \to -\infty\) e \(x \to +\infty\). 

\vspace{.5em}
Iniziamo calcolando il valore della funzione \(f(x)\) quando \(x\) ha come 
valore una \emph{generico infinito negativo} \(M\). 
Possiamo usare un metodo interessante che si usa anche nell'analisi standard:
\[f(M) = \dfrac{3M^2-4M+5}{M^2-2M+3} \stackrel{1}{=} 
 \dfrac{\cancel{M^2} \tonda{3-\frac{4}{M}+\frac{5}{M^2}}}
       {\cancel{M^2} \tonda{1 -\frac{2}{M}+\frac{3}{M^2}}}\]
Ottenendo così un valore finito di cui possiamo ricavare la parte standard:
\[\pst{f(M)} = 
\pst{\dfrac{3-\frac{4}{M}+\frac{5}{M^2}}{1 -\frac{2}{M}+\frac{3}{M^2}}} 
\stackrel{2}{=} 
\dfrac{\pst{3-\frac{4}{M}+\frac{5}{M^2}}}
      {\pst{1 -\frac{2}{M}+\frac{3}{M^2}}} \stackrel{3}{=} 3\]

Oppure possiamo usare la relazione di indistinguibilità 
ottenendo, più rapidamente, lo stesso risultato:
\[f(M) = \dfrac{3M^2-4M+5}{M^2-2M+3} \stackrel{4}{\sim} 
\dfrac{3\cancel{M^2}}{\cancel{M^2}} = 3\]
La relazione di indistinguibilità, 
nel caso produca un valore finito diverso da zero, 
ci dà un valore reale: non occorre usare la funzione parte standard.

Dato che il risultato ottenuto è indipendente dall'infinito negativo \(M\) 
usato:
\[\lim_{x \to -\infty} f(x) = 3\]
Dove i passaggi hanno le seguenti giustificazioni:
\begin{enumerate} [nosep]
\item raccogliamo a fattore comune \(M^2\) e semplifichiamo la frazione;
\item la parte standard di una frazione è il quoziente delle parti standard 
del numeratore e del denominatore, se il denominatore non è un infinitesimo;
\item la parte standard della somma tra un reale e infinitesimi è il reale 
stesso;
\item sostituiamo numeratore e denominatore con espressioni indistinguibili 
(vedi \cite{zanasi1}).
\end{enumerate}
Possiamo osservare che il risultato ottenuto sopra non dipende dal segno di 
\(M\) quindi anche: 
\[\displaystyle \lim_{x \to +\infty} f(x) = 3\]
\begin{center} \limiteallinfinito \end{center}

\section{Conclusione}

È stato dimostrato che il limite \emph{non standard} equivale a quello 
\emph{standard}, ma sono due concetti molto diversi.

\vspace{2em}
\affiancatih{.47}{.47}{
\begin{center} SA \end{center}
Se \(x\) è abbastanza vicino a \(c\), ma non \(c\),
\(f(x)\) si avvicina quanto voglio a \(L\).
}{
\begin{center} NSA \end{center}
Se \(x\) è infinitamente vicino a \(c\), ma non \(c\),
\(f(x)\) è infinitamente vicino a \(L\).
}

\vspace{1em}
\affiancatih{.47}{.47}{
Il limite è alla base delle definizioni di continuità,
derivata, integrale, e viene usato nello studio di funzioni.
}{
Continuità, derivate e integrali \emph{non hanno bisogno} del concetto di 
limite, che può essere utile nello studio delle funzioni.
}

\vspace{1em}
\affiancatih{.47}{.47}{
La definizione \emph{standard} non offre strumenti per calcolare il
limite, ma solo per \emph{verificare se un valore dato è il limite}.
}{
La definizione \emph{non standard} fornisce un \emph{metodo operativo}
che permette di calcolare il limite.
}

\vspace{1em}
\begin{itemize}
\item 
Dato che la trattazione dei limiti non è preliminare allo studio del resto 
dell'analisi questo argomento può essere svolto a piacere:
\begin{itemize} [noitemsep]
\item subito dopo aver svolto gli iperreali, come applicazione del calcolo;
\item all'inizio della trattazione dell'analisi;
\item dopo aver svolto integrali e derivate.
\end{itemize}
\item 
Nella NSA i limiti sono poco considerati dato che è possibile,
sostanzialmente, farne a meno. 
Penso che siano però interessanti da un punto di vista didattico perché 
questo argomento:
\begin{itemize} [noitemsep]
\item viene sempre svolto nelle classi che affrontano l’analisi;
\item è un’interessante applicazione del calcolo con gli iperreali;
\item mette a fuoco alcune caratteristiche del calcolo infinitesimale;
\item aiuta ad abituarsi all'uso di infinitesimi e infiniti.
\end{itemize}
\end{itemize}

\newpage %-----------------------------------------

\section{Dal dibattito}

Durante e dopo il mio intervento sono passate in chat alcune 
osservazioni interessanti che per limiti di tempo non sono 
state valorizzate a sufficienza.
Ne riprendo alcune.

\begin{quoting}
J.: Non è corretto. Per x diverso da 0 e arbitrariamente vicino a zero, 
il valore assoluto di 1/x è "grandissimo", ma il limite per x->0 non esiste.

A: È proprio in virtù della definizione di limite che sono qua ...
\end{quoting}

Due atteggiamenti che dovrebbero sempre esserci: critica e curiosità.

\begin{quoting}
J: questa è la definizione di limite destro, non di limite

J: ah, ok, epsilon può anche essere negativo, come non detto

R: sì, a volte crea un po' di confusione il fatto che un infinitesimo possa 
essere negativo. 
Siamo condizionati da epsilon positivo, ma si possono sempre fare due casi.
\end{quoting}

Quando parlo di infinitesimo, intendo un numero infinitamente vicino a zero
che può essere sia negativo sia positivo.

L'altra possibilità, che semplifica alcuni casi è quella di scegliere 
infinitesimi positivi e esplicitare sempre il segno.

Ovviamente, entrambe le scelte sono buone, basta essere coerenti.

Io preferisco la prima perché mi piacerebbe che alla fine di 5 anni di 
calcolo letterale ai miei alunni potesse sorgere qualche dubbio di fronte 
all'affermazione: ``\(+a\) è positivo e \(-a\) è negativo''. 

\begin{quoting}
(Riferito all'intervento di Cavattoni)
A: Si può dire che il collega abbia fatto analisi non standard senza però 
nominarla?
\end{quoting}

L'analisi non standard, con l'introduzione dei numeri iperreali, dà un 
preciso fondamento logico ai metodi del calcolo infinitesimale. 

Credo che proporre dei metodi funzionanti, ma non del tutto coerenti, 
non sia proprio una bella idea.

L'introduzione dei numeri superreali e un accenno ai numeri iperreali può 
dare un fondamento coerente a quanto trattato e questi metodi possono 
essere utilizzati anche nelle altre questioni di analisi.

\begin{quoting}
N: Io posso dire che dai miei studenti 
il riscontro è stato positivo, nessuno mi ha raccontato di difficoltà 
nell'affrontare Analisi I all'università
\end{quoting}

Penso che i problemi sorgano principalmente da una comprensione più 
superficiale o meccanica della materia.

Una comprensione più profonda permette di spostarsi da un metodo ad un 
altro con più facilità.

\begin{quoting}
J: a livello di scuola superiore credo che tra la trattazione standard 
e quella non standard ci sia una semplice traslitterazione. 
O trattare l'analisi non standard nelle scuole superiori presuppone di 
introdurre i concetti di ultrapotenza, ultrafiltro eccetera?
\end{quoting}

Non sono esperto di storia della matematica, ma mi sembra un po' 
semplicistico parlare di traslitterazione.

I matematici del 19° secolo hanno fatto un enorme sforzo per riuscire 
a rifondare tutti i risultati del calcolo infinitesimale basandolo sui
limiti senza utilizzare infinitesimi e infiniti.

Abraham Robinson a metà del 1900 ha costruito un insieme numerico coerente 
che contiene anche infinitesimi e infiniti permettendo così di recuperare 
tutti i metodi utilizzati prima dell'invenzione dei limiti.

Il compito dell'insegnante è scoprire quali sono i concetti e i metodi che i 
propri alunni possono ``digerire''. 
Io non ho mai proposto gli ultrafiltri.
Se dovessimo andare fino in fondo ad ogni argomento che proponiamo, 
non riuscirei a arrivare in fondo alla teoria dei numeri.

\begin{quoting}
J: Il pericolo è quello di trattare l'analisi non standard come si 
trattano i numeri complessi, cioè dicendo 
``c'è una quantità che ha come quadrato -1'' 
senza mai dimostrare davvero che tale assunzione è sensata.

N: usare l'ANS è molto più semplice concettualmente, non è una semplice 
traslitterazione di quella basata sul limite.

J: si ma si dovrà specificare, da qualche parte, cosa significa formalmente 
infinitesimo, quali sono i possibili modelli di iperreali etc.
\end{quoting}

Ogni nuovo insieme numerico viene introdotto per facilitare la soluzione 
di alcune classi di problemi.
L'introduzione degli iperreali è motivata dalla semplificazione della 
soluzione dei problemi relativi alla pendenza e alle aree.

Se vengono introdotti per bene gli iperreali, si danno tutte le definizioni 
necessarie e si specifica, ovviamente,  cos'è un infinitesimo.

Un buon modello per gli iperreali è la retta, osservata con strumenti ottici 
non standard.

\begin{quoting}
N: capire il concetto di limite non è affatto facile, ed è un grosso 
ostacolo.

P: Il concetto di limite è molto difficile per gli studenti di un liceo non 
scientifico a causa del poco tempo a disposizione. 
Penso che valga la pena utilizzare l’ANS perché più diretta

A: Condivido questo approccio di analisi non standard ma ritengo di 
alleggerire per quanto possibile il formalismo per gli studenti. 

J: sì ma siamo sempre al punto disastroso: a forza di alleggerire il 
formalismo, si finisce per non definire gli oggetti.

F: Ridurre il formalismo non significa secondo me affatto rinunciare al 
rigore logico. 
Provate ad introdurre in un Liceo Linguistico il concetto di limite 
secondo la definizione: i ragazzi muoiono. 
Ho scoperto oggi che io anche se indirettamente ho da tempo utilizzato 
i concetti dell'Analisi Non standard.
\end{quoting}

Il tempo che abbiamo non è poco, è quello che è. Sta all'insegnante 
decidere come utilizzarlo.
Io preferisco utilizzare un mese (in un liceo non scientifico) per 
insegnare ai miei alunni a conoscere e usare i numeri iperreali e così 
fornire loro lo strumento che permette di capire più profondamente e 
risolvere: limiti, derivate e integrali.

Il formalismo nell'uso degli iperreali si riduce a:
\begin{itemize} [nosep]
\item Nuovi simboli \(\IR; \quad \approx; \quad \sim\); 
\item Convenzioni: significato attribuito alle lettere greche minuscole, 
latine maiuscole, altre lettere;
\item Una nuova funzione: \(\pst{}\).
\end{itemize}
Vale la pena di usarlo.

Anch'io quando sono venuto in contatto con la NSA mi sono reso conto 
che quelli erano i metodi che avrei voluto insegnare ai miei allievi.

Insegnare la NSA non significa ridurre il rigore logico, anzi permette di 
dimostrare teoremi con metodi che gli alunni hanno la possibilità di 
comprendere e fare propri.

\begin{comment}

\begin{quoting}

\end{quoting}


17:07:03 From Tommaso Bishop Maruca To Everyone : Personalmente penso che la 
presentazione della NSA alle scuole superiori possa essere un buon modo per 
mostrare agli studenti che cosa sia realmente la matematica.
Purtroppo però non in tutte le università viene trattata quindi va comunque 
affiancata all’analisi classica
17:04:56 From Jacopo D'Aurizio To Everyone : a livello di scuola superiore 
credo che tra la trattazione standard e quella non standard ci sia una 
semplice traslitterazione. O trattare l'analisi non standard nelle scuole 
superiori presuppone di introdurre i concetti di ultrapotenza, ultrafiltro 
eccetera?
17:07:10 From Jacopo D'Aurizio To Everyone : altrimenti è continuare a fare 
quello che si fa nella scuola media, cioè lavorare con cose che non si conoscono 
davvero
17:07:30 From Nicola Fusco To Everyone : lo si può fare in modo assiomatico e 
intuitivo, e fare vedere qualche costruzione di superreali. E' quello che faccio 
io, almeno
17:08:20 From Jacopo D'Aurizio To Everyone : così come si può presentare il 
concetto continuità alla Weierstrass
17:08:26 From Jacopo D'Aurizio To Everyone : traslitterazione, appunto
17:08:26 From Nicola Fusco To Everyone : alle scuole superiori si introducono 
tante cose senza il rigore necessario: qualcuno parla forse di anello dei 
polinomi o di spazi vettoriali? Eppure facciamo calcolo letterale, geometria 
analitica e sistemi lineari
17:08:52 From Jacopo D'Aurizio To Everyone : io parlo sempre di anelli di 
polinomi prima di introdurre C
17:09:01 From Jacopo D'Aurizio To Everyone : detesto che si trattino cose che 
non si sa cosa sono




17:24:40 From Jacopo D'Aurizio To Everyone : ma al di là del ritagliare lo 
spazio, in classe come lo DEFINIAMO cos'è un infinitesimo? Se l'idea dev'essere 
solo quella di dire "esiste", non vedo alcun vantaggio, se non tipografico, nel 
trattare limiti di funzioni razionali (o cose simile) in maniera non standard 
anziché standard

17:32:11 From Roberto Zanasi To Everyone : aggiungerei che non è solo un 
vantaggio tipografico (come scrivere st invece di lim). Per esempio, il 
famigerato dx è una quantità che esiste e si usa all'interno degli integrali. In 
ogni caso, aggiungo che il prof Bonavoglia ha introdotto, in forma un po' 
leggera, l'idea di ultrafiltro per costruire i numeri iperreali alle superiori
17:33:50 From Roberto Zanasi To Everyone : quindi volendo si può non seguire 
solo la via assiomatica
17:34:12 From Marco Vallortigara To Everyone : Mi trovo d’accordo con i Prof. 
Centomo e Degiovanni
17:34:22 From Jacopo D'Aurizio To Everyone : quindi anziché parlare di 
completamento di spazio metrico e epsilon/delta, diamo solo degli abbozzi di 
definizioni allo scopo di scrivere st() anziché lim(). Continuo ad essere 
perplesso.
17:34:55 From Jacopo D'Aurizio To Everyone : ma nessun rigore non ha alcuna 
utilità

17:43:17 From Jacopo D'Aurizio To Everyone : nel primo intervento di stamattina 
è già stato chiarito che la questione "gli infinitesimi hanno la stessa 
struttura d'ordine degli iperreali" è non banale, quindi la storia "è intuitivo" 
non me la bevo
17:43:56 From Tommaso Bishop Maruca To Everyone : Mi piacerebbe intervenire
17:46:01 From Jacopo D'Aurizio To Everyone : quindi per permettere di fare le 
stesse cose, ma con meno caratteri e con fondamenta meno solide?

17:51:25 From Roberto Zanasi To Everyone : fai le stesse cose, però il vantaggio 
è che le fai in modo diverso. Per esempio, non hai bisogno della definizione di 
limite e di fare le verifiche. Altre cose le puoi "vedere" e intuire meglio. Gli 
infinitesimi di ordine superiore che puoi tralasciare in certi casi sono numeri 
veri e propri, e magari ragionare in questo modo è un vantaggio per qualche 
studente. Certo, fai più fatica con le basi, non si può negare questo.  Ma non è 
detto che se uno ci tiene non si possa studiare, la (una) costruzione degli 
iperreali
17:52:04 From Nicola Fusco To Everyone : il limite senza la definizione epsilon 
delta non lo puoi introdurre, e sfido a trovare una classe in cui più della metà 
degli alunni capisca davvero quella definizione, e le fondamenta non sono meno 
solide, Tu come introduci i numeri naturali? li costruisci a partire dalla 
teoria degli insiemi di ZF? Non capisco quale sia il problema nell'accettare 
l'introduzione di una struttura mediante degli assiomi, che sono esatta
17:52:15 From Nicola Fusco To Everyone : mente "un atto di fede"




17:59:33 From Nicola Fusco To Everyone : ma senza definizione il limite cosa è? 
La differenza è questa. l'analisi basata sul limite senza la def epsilon destra 
non la piuoi fare, l'ans senza gli ulgtrafiltri sì
18:00:07 From paolapibiri To Everyone : Grazie a tutti. E’ stata una giornata 
interessantissima…mi auguro di partecipare anche nella prossima
18:00:38 From Daniele Zambelli To Everyone : perché parli di limiti senza 
definizione?
18:00:44 From Fabrizio Giugni To Everyone : siamo tutti nella stessa monade
18:02:03 From Andrea P To Everyone : Per avere notizie sul corso seguo sempre la 
Mathesis? (internet e social?
18:02:23 From Carmen Zeni To Everyone : Ringrazio moltissimo tutti gli 
organizzatori e i relatori per questi validdissimi spunti di riflessione sulla 
didattica della matematica che sicuramente avrò modo di sperimentare volentieri 
a partire dalla terza liceo scientifico.
18:02:43 From Lorenzo Luperi Baglini To Everyone : Provo a dare una risposta di 
tipo diverso alla domanda del Prof. D'Aurizio, ma tenendo presente che non ho 
esperienza diretta di insegnare nelle scuole superiori.
Credo che introdurre gli ultrafiltri sia fuori questione; un modo in cui forse 
proverei a presentare esplicitamente un modello nonstandard, almeno a studenti 
"in gamba", è tramite quoziente di anelli modulo ideali massimali, che forse può 
essere un poco più digeribile.
Però, forse, più che puntare all'introduzione di un modello nonstandard, mi 
limiterei a mostrare che si può formalizzare la nozione di "anello con 
infinitesimi" usando anelli di funzioni razionali (mi pare ne abbia parlato 
Mauro stamattina)

\end{comment}

\begin{thebibliography}{10}
\phantomsection
\addcontentsline{toc}{section}{Bibliografia}

\bibitem[Henle, Kleinberg, 1979]{henle} 
\textsc{James M. Henle, Eugene Kleinberg}, 
\emph{Infinitesimal calculus}, 
New York, 1979

\bibitem[Keisler, 2015]{keisler} 
\textsc{Howard Jerome Keisler},
\emph{Elementary Calculus: An Infinitesimal Approach}, 
\url{www.math.wisc.edu/~keisler/calc.html}

\bibitem[Zambelli 19]{zambelli1} 
\textsc{Daniele Zambelli}, 
Limiti e analisi non standard,
Capitolo 8 di ``\emph{Analisi nonstandard per le scuole superiori, 
IX Giornata di Studio}'', 
Atti del Convegno tenuto a Verona il 5 Ottobre 2019,
% \url{TODO}

\bibitem[AAVV 2020]{zambelli2} 
\textsc{Daniele Zambelli} (a cura di), 
\emph{Matematica Dolce 5}, \\
\url{www.matematicadolce.eu}

\bibitem[Zanasi 21]{zanasi1} 
\textsc{Roberto Zanasi}, 
Non tutti gli infiniti sono uguali, 
Atti del Convegno tenuto a Verona il 13 Marzo 2021,
% \url{TODO}

\bibitem[Ferro 20a]{ferro1} 
\textsc{Ruggero Ferro}, 
Nozione di limite,
Corso PLS ``\emph{Analisi nonstandard per le scuole superiori, 
X Giornata di Studio}'',

\url{
https://bitbucket.org/nsa-verona/nsa_corso_20/downloads/01_nsa_limiti.pdf
}

\bibitem[Ferro 20b]{ferro2} 
\textsc{Ruggero Ferro}, 
Confronto tra la trattazione non standard e standard del
teorema sul limite di una somma,
Corso PLS ``\emph{Analisi nonstandard per le scuole superiori, 
X Giornata di Studio}'',

\url{
https://bitbucket.org/nsa-verona/nsa_corso_20/downloads/02_nsa_limitesomma.pdf
}

\end{thebibliography}
