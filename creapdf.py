#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#-------------------------------python----------------------creapdf.py--#
#                                                                       #
#                           da LaTeX a pdf                              #
#                                                                       #
#--Daniele Zambelli--------------GPL------------------------------2021--#

"""
Manda in compilazione latex il file o i files .tex
passati come argomento.

Uso:

# trasforma in pdf tutti i file .tex presenti nella directory:
$ ./creapdf.py

# trasforma in pdf i file .tex passati come argomento:
$ ./creapdf.py pippo.tex pluto.tex

# opzione -t (test) fa una sola compilazione,
  non cancella i file ausiliari e non produce il pdf monocromatico:
$ ./creapdf.py -t

# opzione -m (mono) esegue la compilazione e produce il pdf monocromatico:
$ ./creapdf.py -m

# opzione -c (clean) pulisce le directory dai file di supporto:
$ ./creapdf.py -c

"""

import sys, getopt
import os

EXTCLEAR = ('.aux', '.gnuplot', '.log', '.nav', '.out', '.snm', '.table',
            '.toc')

def topdf(filename, test, mono):
    """Compile with: pdflatex <filename>."""
    nfile, ext = os.path.splitext(filename)
##    print(filename, '-->', nfile, ext)
    os.system('pdflatex --shell-escape ' + filename)
    if not test:
        os.system('pdflatex --shell-escape ' + filename)
##        os.system('make clean')
    if mono:
        GSLINE = rf"gs -sDEVICE=pdfwrite \
-dProcessColorModel=/DeviceGray \
-dColorConversionStrategy=/Gray \
-dPDFUseOldCMS=false \
-o {nfile}_mono.pdf -f {nfile}.pdf"
        print(f"mono: {GSLINE}")
        os.system(GSLINE)

def clean(path):
    """Clean recursiveli the directory an sub dir."""
    with os.scandir(path) as it:
        for entry in it:
            if True : #not entry.name.startswith('.'):
                if entry.is_file():
                    name, ext = os.path.splitext(entry.name)
                    if ext in EXTCLEAR:
                        print(path, '-->', entry.name)
                        os.remove(entry)
                if entry.is_dir():
##                    print(f"'dir:  ' {entry.name}")
                    clean(entry)
"""
To recursively delete files all .txt in the /tmp directory and all subdirectories under it, the pass the recursive=True argument to the glob() function and use the ``**` pattern:

import os
import glob

files = glob.glob('/tmp/**/*.txt', recursive=True)

for f in files:
    try:
        os.remove(f)
    except OSError as e:
        print("Error: %s : %s" % (f, e.strerror))

Copy

The pathlib module includes two glob functions, glob() and rglob() to match files in a given directory. glob() matches files only in the top level directory. rglob() matches all files in the directory and all subdirectories, recursively. The following example code deletes all .txt files in the /tmp directory:

from pathlib import Path

for f in Path('/tmp').glob('*.txt'):
    try:
        f.unlink()
    except OSError as e:
        print("Error: %s : %s" % (f, e.strerror))
"""

def main(argv):
    inputfile = ''
    outputfile = ''
    try:
        opts, nfiles = getopt.getopt(argv,"hctmi:o:",["ifile=","ofile="])
##        print(opts, args)
    except(getopt.GetoptError):
        print(__doc__)
        sys.exit(2)
    ptest, pmono  = False, False
    print(opts)
##    stop
    for opt, arg in opts:
        if opt == '-h':
             print(__doc__)
             sys.exit()
        elif opt == '-c':
             print("Cancellati:")
             clean('.')
             return
        elif opt == '-t':
            ptest = True
        elif opt == '-m':
            pmono = True
##      elif opt in ("-i", "--ifile"):
##         inputfile = arg
##      elif opt in ("-o", "--ofile"):
##         outputfile = arg
##   print('Input file is "', inputfile)
##   print('Output file is "', outputfile)
    if nfiles == []:
        nfiles = [n_f for n_f in os.listdir('.') if n_f.endswith('.tex')]
    for filename in nfiles:
        topdf(filename, ptest, pmono)

if __name__ == "__main__":
    main(sys.argv[1:])

